/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_newthon_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"

void WET_newton_wet(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_out_io **pout_all, s_carac_wet *pcarac_wet, double dt, FILE *fpout) {

  double pic_wet_temp, pic_wet_max;
  double h_prev, h_next, h_t_prev;
  double surf, theta, eps_wet, pic_max;
  double *sum_condu;
  int nit = 0, nit_wet;
  ;
  int nw, nwet;
  int ng, e, nele;
  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ele_msh *pele;
  nwet = pcarac_wet->nwet;
  theta = pcarac_wet->theta;
  nit_wet = pcarac_wet->nit_wet;
  eps_wet = pcarac_wet->eps_wet;
  if (nit_wet == 0)
    nit_wet = 100; // if > 0;
  if (eps_wet == 0.0)
    eps_wet = 0.000001;
  // WET_ini_leakage_all(pcarac_wet,fpout);
  // WET_get_leakage_all(pcarac_wet,dt,fpout);

  for (nw = 0; nw < nwet; nw++) {
    nit = 0;
    pwet = pcarac_wet->p_wet[nw];

    surf = pwet->surf;
    h_prev = pwet->phydro->h[ITER_WET];
    h_t_prev = pwet->phydro->h[ITER_WET];
    LP_printf(fpout, "h_ini = %f h_prev = %f eps = %f\n", pwet->phydro->h[INI_WET], pwet->phydro->h[ITER_WET], eps_wet);
    sum_condu = WET_calc_condu_sum_wet(pwet, fpout);
    while (nit < nit_wet) {
      h_next = h_prev - (h_prev - h_t_prev - ((pwet->phydro->q[WET_PRE] - pwet->phydro->q[WET_EVAP]) * dt + sum_condu[SUM_PROD_CONDU] - h_prev * sum_condu[SUM_CONDU]) * dt / surf) / (1 + sum_condu[SUM_CONDU] * dt / surf);
      pic_wet_temp = h_next - h_prev;
      if (fabs(pic_wet_temp) < eps_wet)
        break;
      h_prev = h_next;
      nit++;
    }
    WET_refresh_dirichlet_lake(pwet, h_next, fpout);
    pwet->phydro->h[ITER_WET] = pwet->phydro->h[T_WET];
    pwet->phydro->h[T_WET] = h_next;
    LP_printf(fpout, "H_lake for id_wet %d at prev time step = %f and %f for next time step\n", nw + 1, pwet->phydro->h[ITER_WET], pwet->phydro->h[T_WET]);
    if (nit == nit_wet)
      LP_warning(fpout, "h_lake non converge after %d newton iterations ! pic_wet_temp = %f\n", nit, pic_wet_temp);
  }

  return;
}