#-------------------------------------------------------------------------------
# 
# LIBRARY NAME: libwet
# FILE NAME: Makefile
# 
# CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
# (*) Sorbonne University
#
# LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations 
# (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere 
# and aquifer system.
#
# Library developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the libwet Library.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Library and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/

PROGRAMME =  l$(NAME).$(VPROG)
PATH_INST= ${LIB_HYDROSYSTEM_PATH}

NAME = wet
VPROG=$(V_WET)
NAMELIB=lib$(NAME)

SHELL = /bin/bash

V_WET=0.12

SOURCESLIB.c = manage_lake.c itos.c math_lake.c manage_carac.c manage_geom.c manage_hydro_wet.c manage_temp_lake.c manage_condu_wet.c manage_boundary_wet.c manage_mb_wet.c manage_newton_wet.c WET_solve_wet.c manage_outputs.c manage_mto_wet.c

SOURCES.h = WET.h  

CC=gcc

GCC_VER=$(shell $(CC) -v 2>&1|tail -1|awk '{print $$3}'|awk -F "." '{print $$1$$2$$3}')
COMPILO=$(shell echo $(CC) | awk -F "/" '{print $$NF}')
#YACC= bison -y 
#LEX= flex
#YFLAGS= -dv 
#LFLAGS= -v

#Configure dependencies
#Versions
V_PC=0.04
V_LP=1.22
V_TS=1.58
V_GC=0.02
V_CHR=0.02
V_IO=0.03
V_SPA=0.02
V_RSV=0.01
V_AQ=0.27
V_HYD=0.07
V_MSH=0.07
V_FP=0.02
#Paths for Include
INCL_GC=-I$(PATH_INST)/libgc/src/
INCL_PC=-I$(PATH_INST)/libpc/src/
INCL_LP=-I$(PATH_INST)/libprint/src/
INCL_TS=-I$(PATH_INST)/libts/src/
INCL_CHR=-I$(PATH_INST)/libchronos/src/
INCL_MSH=-I$(PATH_INST)/libmesh/src/
INCL_IO=-I$(PATH_INST)/libio/src/
INCL_SPA=-I$(PATH_INST)/libspa/src/
INCL_AQ=-I$(PATH_INST)/libaq/src/
INCL_HYD=-I$(PATH_INST)/libhyd/src/
INCL_MSH=-I$(PATH_INST)/libmesh/src/
INCL_FP=-I$(PATH_INST)/libfp/src/
INCL_RSV=-I$(PATH_INST)/librsv/src/

# Attention, l'ordre est important!
LIBS= -L$(PATH_INST) -laq$(V_AQ)_$(COMPILO) -lmesh$(V_MSH)_$(COMPILO) -lhyd$(V_HYD)_$(COMPILO) -lspa$(V_SPA)_$(COMPILO) -lfp$(V_FP)_$(COMPILO) -lrsv$(V_RSV)_$(COMPILO) -lio$(V_IO)_$(COMPILO) -lchronos$(V_CHR)_$(COMPILO) -lgc$(V_GC)_$(COMPILO) -lts$(V_TS)_$(COMPILO)  -lpc$(V_PC)_$(COMPILO) -lpc$(V_PC)_$(COMPILO) -lprint$(V_LP)_$(COMPILO) -lc -lm

OPTCOMP = -g
#OPTW= -Wall
OPTW= -Wno-write-strings #-fpermissive
#OPTD = -DCOUPLED 
OPTD = -DOMP $(OPTOMP) -DCOUPLED 
#OPTD = -DTS_DEBUG
#OPTD = -DSO_OLD

#This option indicates the version of GCC to the pre-compiler to properly compile the lexical.l
CFLAGSD = -DGCC$(GCC_VER) #Mettre en commentaire pour certaine version de lex
#OPTOMP= -fopenmp
#OPTD = -DOMP $(OPTOMP)
#OPTD = -DCOUPLED
CFLAGS_GLOBAUX= $(OPTCOMP) $(OPTW) 
CFLAGS= $(CFLAGS_GLOBAUX) $(OPTD) $(CFLAGSD)


INCLUDES = -I/usr/local/gcc-4.7.2/include -I/usr/local/gcc-4.7.2/lib/gcc/x86_64-unknown-linux-gnu/4.7.2/include/#for system using gcc older than 4.x
#INCLUDES = -I/usr/local/include

CPPFLAGS= $(INCL_GC) $(INCL_LP) $(INCL_TS) $(INCL_PC) $(INCL_IO) $(INCL_CHR) $(INCL_RSV) $(INCL_FP) $(INCL_SPA) $(INCL_NSAT) $(INCL_HYD) $(INCL_AQ) $(INCL_MSH)

# Variables déduites des précédentes:
OBJPARSER=$(SOURCES.y:.y=.o) $(SOURCES.l:.l=.o)
OBJLIB=$(SOURCESLIB.c:.c=.o)
OBJP=$(SOURCESP.c:.c=.o)
OBJ=$(OBJPARSER) $(OBJLIB) $(OBJP)

all: $(PROGRAMME)

%.o : %.c
	@echo 'Creating $(@)...'
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $(basename $@).c -o $@ 

$(PROGRAMME) :  $(OBJ) 
	@echo 'Creating $(@)...'
	#$(CC) -o $(PROGRAMME)  $(OBJ)  $(LIBS)
	$(FC)  $(CFLAGS) -o $@ $(OBJ) $(LIBS)

install : $(PROGRAMME)
	rm ~/executables/$(NAME)
	ln -s ~/Programmes/$(NAME)/trunk/src/$(NAME)$(VPROG) ~/executables/$(NAME)
	@echo "You can start" $(NAME)$(VPROG) "from everywhere with the command >" $(NAME) "*.COMM *.log"

version: 
	./update_version.sh $(VPROG)

# Pour nettoyer quand on change de machine:
clean:
	rm -f $(OBJ) $(LINTFILES) $(PROGRAMME) core y.* apoub* manage

allpub: $(PROGRAMME)
	rm *.c *.h Makefile

libst :  $(OBJLIB) 
	@echo "Creating static library $(NAMELIB)$(VPROG).a"
	ar rv $(NAMELIB)$(VPROG)_$(COMPILO).a $(OBJLIB) 
	mv $(NAMELIB)$(VPROG)_$(COMPILO).a $(PATH_INST)/$(NAMELIB)$(VPROG)_$(COMPILO).a

lib : libst

libpub :  $(libst) 
	rm *.c Makefile

test : 
	./l$(NAME).$(V_AQ) test_simple.COMM test.log

print : 
	@echo "Compiling l$(NAME)$(VPROG)"
	 @echo "$(CC) version : $(GCC_VER)"
	 @echo "INCLUDES sat to $(INCLUDES)"
	 @echo "compiling options $(CFLAGS)"