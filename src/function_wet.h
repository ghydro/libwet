/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: function_wet.h
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// in manage_carac.c
s_carac_wet *WET_create_carac_lake();
s_lake_wet **WET_ini_tab_lake(int, FILE *);
void WET_print_carac_wet(s_carac_wet *, FILE *);

// in manage_geom.c
s_geom_wet *WET_ini_geom();
s_geom_wet *WET_chain_geom_bck(s_geom_wet *, s_geom_wet *);
s_geom_wet *WET_secured_chain_geom_bck(s_geom_wet *, s_geom_wet *);
s_geom_wet *WET_chain_geom_fwd(s_geom_wet *, s_geom_wet *);
s_geom_wet *WET_secured_chain_geom_fwd(s_geom_wet *, s_geom_wet *);
s_geom_wet **WET_tab_geom(s_geom_wet *, int, FILE *);
s_geom_wet *WET_browse_geom(s_geom_wet *, int);
s_geom_wet **WET_ini_tab_geom(int, FILE *);
s_id_io **WET_get_list_geom(s_temp_wet **, s_cmsh *, int, FILE *);
void WET_define_geom(s_carac_wet *, s_carac_aq *, s_temp_wet **, FILE *);
// void WET_define_vol_surf_geom(s_carac_wet *,FILE *);
void WET_calc_surf_wet(s_carac_wet *, FILE *);

// in manage_lake.c
s_lake_wet *WET_ini_lake();
s_lake_wet *WET_chain_lake_bck(s_lake_wet *, s_lake_wet *);
s_lake_wet *WET_secured_chain_lake_bck(s_lake_wet *, s_lake_wet *);
s_lake_wet *WET_chain_lake_fwd(s_lake_wet *, s_lake_wet *);
s_lake_wet *WET_secured_chain_lake_fwd(s_lake_wet *, s_lake_wet *);
s_lake_wet *WET_browse_lake(s_lake_wet *, int);
s_lake_wet **WET_tab_lake(s_lake_wet *, int, FILE *);

// in manage_temp_lake.c
s_temp_wet *WET_ini_temp_lake();
s_temp_wet *WET_create_temp_lake(int, char *, int);
s_temp_wet *WET_chain_temp_bck(s_temp_wet *, s_temp_wet *);
s_temp_wet *WET_secured_chain_temp_bck(s_temp_wet *, s_temp_wet *);
s_temp_wet *WET_chain_temp_fwd(s_temp_wet *, s_temp_wet *);
s_temp_wet *WET_secured_chain_temp_fwd(s_temp_wet *, s_temp_wet *);
s_temp_wet **WET_tab_temp(s_temp_wet *, int, FILE *);
s_temp_wet *WET_browse_temp(s_temp_wet *, int);
s_temp_wet **WET_ini_tab_temp_lake(int, FILE *);
s_temp_wet *WET_free_temp(s_temp_wet *);
s_temp_wet *WET_free_temp_serie(s_temp_wet *, FILE *);

// in manage_hydro_wet.c
s_hydro_wet *WET_ini_hydrowet();
void WET_ini_hini_wet(s_carac_aq *, s_carac_wet *, FILE *);

// in manage_condu_wet.c
s_condu_wet *WET_ini_condu();
s_condu_wet **WET_ini_tab_condu(int, FILE *);
// void WET_define_condu_wet(s_cmsh *, s_carac_wet *,FILE *);
void WET_define_condu_wet(s_cmsh *, s_carac_wet *, s_chronos_CHR *, FILE *); // TV 17/12/2019
double *WET_calc_condu_sum_wet(s_lake_wet *, FILE *);                        // 25-05-2016

// in manage_boundray_wet.c
void WET_define_dirichlet(s_cmsh *, s_carac_wet *, FILE *);

// in math_lake.c
void WET_ini_leakage_all(s_carac_wet *, FILE *);
void WET_ini_leakage(s_lake_wet *, FILE *);
void WET_get_leakage(s_lake_wet *, int, double, double *, double, double, FILE *);
void WET_get_leakage_all(s_carac_wet *, int, double, FILE *);
double WET_calc_h_lake(s_lake_wet *, double, double, int, FILE *);
// void WET_get_recharge(s_lake_wet **, int , FILE *);
void WET_refresh_h_lake(s_carac_wet *, int, double, int, FILE *);
void WET_refresh_dirichlet_lake(s_lake_wet *, double, FILE *);
double WET_min(double, double);
double WET_max(double, double);

// manage_newton_wet.c
void WET_newton_wet(s_carac_aq *, s_chronos_CHR *, s_out_io **, s_carac_wet *, double, FILE *);

// in manage_mb_wet.c
int *WET_face_aq_wet(s_lake_wet *, FILE *);
s_face_msh **WET_ini_pfaces_wet(int, FILE *);
s_ele_msh **WET_ini_peles_wet(int, FILE *);
int WET_check_face_aq_wet(s_geom_wet *, int, FILE *);
void WET_define_face_ele_aq_wet(s_carac_aq *, s_carac_wet *, FILE *); // 25-05-2016
void WET_calc_mb_face_wet(s_carac_aq *, s_lake_wet *, s_chronos_CHR *, s_out_io **, FILE *);
void WET_reinit_mb_face_wet(s_lake_wet *, FILE *);
void WET_refresh_ele_h_wet(s_lake_wet *, FILE *);
void WET_refresh_ele_h_wet_all(s_carac_wet *, FILE *);
// void WET_get_in_outflux_wet(s_lake_wet *,FILE *);

// in WET_solve_wet.c
double WET_solve_diffusiv_pic(s_carac_aq *, s_carac_wet *, int, int, s_chronos_CHR *, s_out_io *, s_clock_CHR *, FILE *);
double WET_calc_pic(s_carac_wet *, FILE *);
void WET_solve_diffusiv(s_carac_aq *, s_carac_wet *, int, int, s_chronos_CHR *, s_out_io *, s_clock_CHR *, s_out_io **, FILE *); // SW 05/07/2016

// in manage_outputs.c - NG : 01/11/2024 - All functions rewritten using libio
void WET_print_headers(FILE *, int, FILE *);
void WET_print_outputs(s_chronos_CHR *, double, s_carac_wet *, s_out_io *, int, FILE *);
void WET_write_h_wet(double, s_out_io *, s_carac_wet *, FILE *);
void WET_write_mb_wet(double, double, s_out_io *, s_carac_wet *, FILE *);

// in itos.c
char *WET_name_attributs(int);
char *WET_name_param_attributs(int);

// in manage_mto_wet.c
void WET_ini_mto_wet(s_carac_wet *, int, FILE *);
void WET_free_mto_wet(s_carac_wet *, FILE *);
void WET_open_mto_file(s_carac_wet *, char *, char **, int, FILE *);
void WET_calc_penman_transient_mto(s_carac_wet *, int, FILE *);
void WET_read_evap_mto(s_carac_wet *, int, FILE *); // TV 23/01/2020
void WET_series_to_mean_mto(s_carac_wet *, FILE *);
double *WET_get_evp_p_from_date(s_lake_wet *, int, int, FILE *);
void WET_print_evap_penman_to_file(s_carac_wet *, FILE *);
void WET_calc_penman_steady_mto(s_carac_wet *, FILE *);