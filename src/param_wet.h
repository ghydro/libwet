/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: param_wet.h
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @def CODE_WET
 * @brief Libwet error code
 */
#define CODE_WET CODE_TS

/**
 * @def EPS_LIMIT_WET
 * @brief TBD
 */
#define EPS_LIMIT_WET 1.e-14

/**
 * @def EPS_DEFAULT_WET
 * @brief TBD
 */
#define EPS_DEFAULT_WET 1.e-7

/**
 * @def NIT_DEFAULT_WET
 * @brief TBD
 */
#define NIT_DEFAULT_WET 300

/**
 * @def VERSION_WET
 * @brief Library version ID
 */
#define VERSION_WET 0.12

#define PATM 101.325      // Kpa pression atmospherique
#define ALBEDO 0.08       // pour l'eau
#define EMISSIVITY 0.95   // emissivity for water 0.92-0.95
#define SIGMA 5.670373e-8 // la constante de stefan-Boltzemann
#define T_REF 273.16

/**
 * @enum dim_wet
 * @brief TBD
 */
enum dim_wet { X_WET, Y_WET, Z_WET, ND_WET };

/**
 * @enum orientation_wet
 * @brief TBD
 */
enum orientation_wet { ONE_WET, TWO_WET, NFACE_WET }; // for phydro_wet conductance

/**
 * @enum beginning_end_wet
 * @brief TBD
 */
enum beginning_end_wet { BEGINNING_WET, END_WET, NEXTREMA_WET };

/**
 * @enum answer_wet
 * @brief TBD
 */
enum answer_wet { NO_WET, YES_WET, FLAG_WET };

/**
 * @enum type_bilan
 * @brief Terms used for water balance, influx_v vertical, influx_H, horizontal
 */
enum type_bilan { WET_PRE, WET_EVAP, WET_RUN, WET_Q, WET_LEAKAGE, WET_INFLUX, WET_INFLUX_V, WET_INFLUX_H, WET_OUTFLUX, WET_OUTFLUX_V, WET_OUTFLUX_H, WET_DEL_V, WET_NTYPE };

/**
 * @enum output_hwet
 * @brief Enumeration which lists the records in the H_WET output file
 * @internal NG : 01/11/2024
 */
enum output_hwet { H_OUT_WET, NH_OUT_WET };

/**
 * @enum outputs_mbwet
 * @brief Enumeration which lists the records in the MB_WET output file
 */
enum outputs_mbwet { MBWET_PRE, MBWET_EVAP, MBWET_RUN, MBWET_INFLUX, MBWET_INFLUX_V, MBWET_INFLUX_H, MBWET_OUTFLUX, MBWET_OUTFLUX_V, MBWET_OUTFLUX_H, MBWET_LEAKAGE, MBWET_DEL_V, MBWET_REL_ERROR, N_MB_OUT_WET };

/**
 * @enum kindof_mesh_att_wet
 * @brief Terms for input.y
 */
enum kindof_mesh_att_wet { WET_ID_ATT, WET_EVAP_ATT, WET_PREP_ATT, WET_H_INI_ATT, WET_Z_MAX_ATT, WET_Z_MIN_ATT, NMESH_ATT_WET };

/**
 * @enum kindof_param_wet
 * @brief Terms for input.y
 */
enum kindof_param_wet { WET_NUM_ATT, WET_EPS_ATT, WET_NIT_ATT, WET_THETA_ATT, WET_ALPHA_ATT };

/**
 * @enum timing_ref_wet
 * @brief Terms for input.y
 */
enum timing_ref_wet { INI_WET, T_WET, ITER_WET, NTIME_WET };

/**
 * @enum timing_ref_wet
 * @brief Type elements in a lake UP et DOWN
 */
enum type_ele_wet { UP_WET, WET_DOWN, WET_NTYPE_ELE };

/**
 * @enum type_sum_cond
 * @brief TBD
 */
enum type_sum_cond { SUM_CONDU, SUM_PROD_CONDU, TYPE_SUM_WET };

/**
 * @enum type_face_wet
 * @brief TBD
 * @internal vertical and horizontal face TV 25/09/2019: moving from one to four value of lateral cond
 */
enum type_face_wet { V_WET, H_WET, NTYPE_FACE_WET };

/**
 * @enum type_face_condu_wet
 * @brief TBD
 */
enum type_face_condu_wet { V_WET_W, V_WET_E, V_WET_S, V_WET_N, H_WET_B, NTYPE_FACE_COND_WET }; // TV 25/09/2019

/**
 * @enum type_mto_wet
 * @brief TBD
 */
enum type_mto_wet { PRECIPIT_WET, EVAPORATION_WET, NTYPE_MTO_WET };

/*terms uesed for bathmetry volume-stage lake*/
// enum aquifer_bot_top_wet { A_BOT_WET, A_TOP_WET,ABT_WET };
// enum volume_bot_top_wet { V_BOT_WET, V_TOP_WET,VBT_WET };