/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: math_lake.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
// #include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"

/** \fn void WET_get_recharge(s_lake_wet **p_wet, int nwet, FILE *fp)
 * \brief find recharge in m3/s for all lakes
 *  q[WET_PRE] precipitation
 */
/*
void WET_get_recharge(s_lake_wet **p_wet, int nwet, FILE *fp)
{
   int nw, e;
   for(nw = 0; nw < nwet; nw++)
   {
      for(e = 0; e < p_wet[nw]->nele[UP_WET]; e++)
      {
          p_wet[nw]->q[WET_PRE] += p_wet[nw]->p_ele[UP_WET][e]->phydro->psource[RECHARGE]->pft->ft*p_wet[nw]->p_ele[UP_WET][e]->pdescr->surf;
      }
      //LP_printf(fp,"Recharge for id_wet %d = %.10f\n",nw+1, p_wet[nw]->q[WET_PRE]);
   }
   return;
}
*/
void WET_ini_leakage_all(s_carac_wet *pcarac_wet, FILE *fp) {
  s_lake_wet *pwet;

  int nwet;
  int nw;

  nwet = pcarac_wet->nwet;

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];

    pwet->phydro->q[WET_LEAKAGE] = 0.0;
    pwet->phydro->q[WET_INFLUX] = 0.0;
    pwet->phydro->q[WET_INFLUX_V] = 0.0;
    pwet->phydro->q[WET_INFLUX_H] = 0.0;
    pwet->phydro->q[WET_OUTFLUX] = 0.0;
    pwet->phydro->q[WET_OUTFLUX_V] = 0.0;
    pwet->phydro->q[WET_OUTFLUX_H] = 0.0;
  }
  // LP_printf(fp,"Initialize leakege! \n");
  return;
}

void WET_ini_leakage(s_lake_wet *pwet, FILE *fp) {
  pwet->phydro->q[WET_LEAKAGE] = 0.0;
  pwet->phydro->q[WET_INFLUX] = 0.0;
  pwet->phydro->q[WET_INFLUX_V] = 0.0;
  pwet->phydro->q[WET_INFLUX_H] = 0.0;
  pwet->phydro->q[WET_OUTFLUX] = 0.0;
  pwet->phydro->q[WET_OUTFLUX_V] = 0.0;
  pwet->phydro->q[WET_OUTFLUX_H] = 0.0;
  return;
}

/**\fn void WET_get_leakage(s_lake_wet **p_ele, int nele_up, FILE *fp)
 * \brief find leakage between lake and aquifer
 * return leakage
 */
void WET_get_leakage_all(s_carac_wet *pcarac_wet, int calc_state, double dt, FILE *fp) {
  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ele_msh *pele;
  int nwet, ngeom, nele;
  int e, nw, ng;
  int idir, itype;
  double h_lake;
  double theta_wet;
  double *sum_condu;
  theta_wet = pcarac_wet->theta;
  WET_ini_leakage_all(pcarac_wet, fp);
  nwet = pcarac_wet->nwet;

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    h_lake = pwet->phydro->h[T_WET];
    sum_condu = WET_calc_condu_sum_wet(pwet, fp);
    WET_get_leakage(pwet, calc_state, theta_wet, sum_condu, h_lake, dt, fp);
  }
  return;
}

void WET_get_leakage(s_lake_wet *pwet, int calc_state, double theta_wet, double *sum_condu, double h_lake_next, double dt, FILE *fp) {

  double erreur, deletav_wet;
  double seepage, flux;
  int *nface;
  int nf_V, nf_H;
  s_face_msh **p_faces_V;
  s_face_msh **p_faces_H;
  s_ele_msh **pneigh_ele_V;
  s_ele_msh **pneigh_ele_H;

  WET_ini_leakage(pwet, fp);
  if (calc_state == STEADY)
    theta_wet = 0.0;
  nface = pwet->nface;
  p_faces_V = pwet->p_faces_V;
  p_faces_H = pwet->p_faces_H;
  pneigh_ele_V = pwet->pneigh_ele_V;
  pneigh_ele_H = pwet->pneigh_ele_H;

  deletav_wet = (h_lake_next - pwet->phydro->h[T_WET]) * pwet->surf;

  seepage = sum_condu[SUM_PROD_CONDU] - ((1 - theta_wet) * pwet->phydro->h[T_WET] + theta_wet * h_lake_next) * sum_condu[SUM_CONDU];
  pwet->phydro->q[WET_LEAKAGE] = seepage;
  // for(nf_V = 0; nf_V < nface[V_WET]; nf_V++)
  for (nf_V = 0; nf_V < nface[V_WET_W]; nf_V++) {
    flux = p_faces_V[nf_V]->phydro->pcond->conductance * (pneigh_ele_V[nf_V]->phydro->h[T] - ((1 - theta_wet) * pwet->phydro->h[T_WET] + theta_wet * h_lake_next));
    if (flux > 0.0) {
      pwet->phydro->q[WET_INFLUX_V] += flux;
      // LP_printf(fp,"id_ele = %d, h_t = %f, influx = %f\n",pneigh_ele[nf]->id[ABS_MSH],pneigh_ele[nf]->phydro->h[T],flux);
    } else {
      pwet->phydro->q[WET_OUTFLUX_V] += fabs(flux);
      // LP_printf(fp,"id_ele = %d, h_t = %f, outflux = %f\n",pneigh_ele[nf]->id[ABS_MSH],pneigh_ele[nf]->phydro->h[T],fabs(flux));
    }
  }
  for (nf_H = 0; nf_H < nface[H_WET]; nf_H++) {
    flux = p_faces_H[nf_H]->phydro->pcond->conductance * (pneigh_ele_H[nf_H]->phydro->h[T] - ((1 - theta_wet) * pwet->phydro->h[T_WET] + theta_wet * h_lake_next));
    if (flux > 0.0) {
      pwet->phydro->q[WET_INFLUX_H] += flux;
      // LP_printf(fp,"id_ele = %d, h_t = %f, influx = %f\n",pneigh_ele[nf]->id[ABS_MSH],pneigh_ele[nf]->phydro->h[T],flux);
    } else {
      pwet->phydro->q[WET_OUTFLUX_H] += fabs(flux);
      // LP_printf(fp,"id_ele = %d, h_t = %f, outflux = %f\n",pneigh_ele[nf]->id[ABS_MSH],pneigh_ele[nf]->phydro->h[T],fabs(flux));
    }
  }

  pwet->phydro->q[WET_DEL_V] = deletav_wet;
  // erreur = ((pwet->phydro->q[WET_PRE] - pwet->phydro->q[WET_EVAP])*pwet->surf + seepage)*dt - deletav_wet;
  // LP_printf(fp,"id_wet = %d\tdeletav_wet = %f\tleakege + p - e  = %.10f\n",pwet->id_wet,deletav_wet,erreur);
  return;
}

/**\fn double WET_calc_h_lake(s_carac_aq *pchar_aq,s_lake_wet **p_wet, double dt,FILE *fp)
 * \brief calculate h_lake for next time step
 *  refresh h_lake of all elements in all lakes
 */
double WET_calc_h_lake(s_lake_wet *pwet, double dt, double theta, int calc_state, FILE *fp) // 25-05-2016
{
  double h_lake, h_lake_next;
  double surf;
  double theta_wet;
  double *sum_condu;

  double h_lake_prev;
  theta_wet = theta;
  surf = pwet->surf;

  h_lake_prev = pwet->phydro->h[T_WET];
  sum_condu = WET_calc_condu_sum_wet(pwet, fp);
  // LP_printf(fp,"yes you are right! %f\n",sum_condu);
  if (calc_state == STEADY)
    h_lake_next = ((pwet->phydro->q[WET_PRE] - pwet->phydro->q[WET_EVAP]) * surf + sum_condu[SUM_PROD_CONDU]) / sum_condu[SUM_CONDU];
  else
    h_lake_next = (h_lake_prev + dt * ((pwet->phydro->q[WET_PRE] - pwet->phydro->q[WET_EVAP]) * surf + (sum_condu[SUM_PROD_CONDU] - (1 - theta_wet) * h_lake_prev * sum_condu[SUM_CONDU])) / surf) / (1 + theta_wet * dt * sum_condu[SUM_CONDU] / surf);
  if (calc_state != STEADY) {
    WET_get_leakage(pwet, calc_state, theta_wet, sum_condu, h_lake_next, dt, fp);
  }
  return h_lake_next;
}

/**\fn double WET_calc_h_lake(s_carac_aq *pchar_aq,s_lake_wet **p_wet, double dt,FILE *fp)
 * \brief calculate h_lake for next time step
 *  refresh h_lake of all elements in all lakes
 */
/*void WET_refresh_h_lake(s_carac_wet *pcarac_wet,double dt, FILE *fp)
{
   s_lake_wet *pwet;
   s_geom_wet *pgeom;
   s_ele_msh *pele;
   int nwet,ngeom,nele;
   int e, nw,ng;
   WET_ini_leakage(pcarac_wet,fp);
   WET_get_leakage(pcarac_wet,dt,fp);
   nwet = pcarac_wet->nwet;
  for(nw = 0; nw < nwet; nw++)
  {
    pwet=pcarac_wet->p_wet[nw];
    ngeom = pwet->ngeom;
    pwet->phydro->h[T_WET]=pwet->phydro->h[ITER_WET];
    pwet->phydro->h[ITER_WET]=WET_calc_h_lake(pwet,dt,fp);
    for(ng = 0; ng < ngeom; ng++)
      {
        pgeom=pwet->p_geom[ng];
        nele = pgeom->nele;
        for(e = 0; e < nele; e++)
          { pele = pgeom->p_ele[e];

            //pele->phydro->h[ITER] = pwet->phydro->h[ITER_WET]; // refresh h_lake of all elements in nw+1 lake for next time step;
            //pele->phydro->h[INTERPOL] = pwet->phydro->h[ITER_WET];
            //pele->phydro->h[ITER_INTERPOL] = pwet->phydro->h[ITER_WET];;
            pele->phydro->pbound->pft->ft = pwet->phydro->h[ITER_WET]; //to modify value of DIRICLET ??????
          }
        fprintf(stdout,"H_lake for id_wet %d at prev time step = %f and %f for next time step\n",nw+1,pwet->phydro->h[T_WET], pwet->phydro->h[ITER_WET]);
      }

   }

 return;
}*/
void WET_refresh_h_lake(s_carac_wet *pcarac_wet, int nday, double dt, int calc_state, FILE *fp) // non iteration mais direct apres chaque pas de temps 25-05-2016
{
  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ele_msh *pele;
  int nwet, ngeom, nele;
  int e, nw, ng;
  int length_mto;
  double h_lake_next, theta_wet;
  double Z_max, Z_min;
  double *value; // evaporation and precipitation values in t day
  nwet = pcarac_wet->nwet;
  theta_wet = pcarac_wet->theta;
  length_mto = pcarac_wet->length_mto; // length climate data

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    Z_max = pwet->phydro->Z_max;
    Z_min = pwet->phydro->Z_min;
    h_lake_next = WET_calc_h_lake(pwet, dt, theta_wet, calc_state, fp);

    pwet->phydro->h[ITER_WET] = pwet->phydro->h[T_WET];
    pwet->phydro->h[T_WET] = h_lake_next;
    WET_refresh_dirichlet_lake(pwet, h_lake_next, fp);

    if ((Z_max - Z_min) > EPS_LIMIT_WET) {
      if ((h_lake_next - Z_max) >= EPS_LIMIT_WET) {
        LP_warning(fp, "Lake stage computed %f is greater than the max lake stage %f\n", h_lake_next, Z_max);
      } else if ((h_lake_next - Z_min) <= -EPS_LIMIT_WET)
        LP_warning(fp, "Lake stage computed %f is less than the max lake stage %f\n", h_lake_next, Z_min);
    }

    // fprintf(stdout,"H_lake for id_wet %d at prev time step = %f and %f for next time step\n",pwet->id_wet,pwet->phydro->h[ITER_WET], pwet->phydro->h[T_WET]);
  }

  return;
}
void WET_refresh_dirichlet_lake(s_lake_wet *pwet, double h_next, FILE *fpout) {

  s_geom_wet *pgeom;
  s_ele_msh *pele;
  int ngeom, nele;
  int e, ng;

  ngeom = pwet->ngeom;
  for (ng = 0; ng < ngeom; ng++) {
    pgeom = pwet->p_geom[ng];
    nele = pgeom->nele;
    for (e = 0; e < nele; e++) {
      pele = pgeom->p_ele[e];
      // pele->phydro->h[T] = pwet->phydro->h[ITER_WET]; // refresh h_lake of all elements in nw+1 lake for next time step;
      pele->phydro->h[T] = pwet->phydro->h[T_WET];
      pele->phydro->pbound->pft->ft = h_next; // to modify value of DIRICLET ?????? // 25-05-2016
      // pele->phydro->h[INTERPOL] = h_next;
      // pele->phydro->h[ITER_INTERPOL] = pwet->phydro->h[ITER_WET];
    }
  }

  return;
}

double WET_min(double a, double b) { return a > b ? b : a; }

double WET_max(double a, double b) { return a < b ? b : a; }
