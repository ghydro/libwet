/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: WET_solve_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
// #include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
double WET_solve_diffusiv_pic(s_carac_aq *pchar_aq, s_carac_wet *pcarac_wet, int nday, int calc_state, s_chronos_CHR *chronos, s_out_io *pout, s_clock_CHR *clock, FILE *fpout) {

  int err, i;
  double pic_max_wet;
  double t, dt = DT_CHR;
  s_gc *pgc;
  s_cmsh *pcmsh;
  s_param_aq *param;
  pic_max_wet = 0.0;
  pgc = pchar_aq->calc_aq;
  pcmsh = pchar_aq->pcmsh;
  param = pchar_aq->settings;

  if (chronos != NULL) {
    dt = chronos->dt;
  }

  CHR_begin_timer();
  AQ_calc_vector_b(pchar_aq, chronos, chronos->t[CUR_CHR], fpout);
  clock->time_spent[MAT_FILL_AQ] += CHR_end_timer();
  // BL ATTENTION il faut recompiler libmesh !!!!!!!!!!!
  // BL faire fonction solve_unconfined ou boucle de picard pour ça faire une nouvelle fonction refresh qui ne modifie que [T] + ajout de la fonction recalc_Transm + recalcul transm_face_unconfined (sur layer unconfined fonction à faire ou ancienne à modifier) +   dans mat6 faudrait il mettre une condition sur le layer unconfined (sorte de calc_mat6 unconfined) pour ne pas tout recalculer tout le
  // reste devrait etre identique le solve confined est juste GC_solve_gc!!
  //	}
  CHR_begin_timer();

  // NG : 11/06/2021 : GC_configure_gc() added to update GC parameters according to the time-step or iteration we're at.
  GC_configure_gc(pgc, &nday, fpout);
  GC_solve_gc(pgc, AQ_GC, fpout);

  clock->time_spent[MAT_SOLVE_AQ] += CHR_end_timer();
  // system("rm /home/blabarthe/Programmes/cawaqs/trunk/Cmd_files/mac5*");
  CHR_begin_timer();
  AQ_refresh_H_interpol(pchar_aq, chronos, pout, fpout);
  WET_refresh_h_lake(pcarac_wet, nday, dt, calc_state, fpout);
  pic_max_wet = AQ_calc_pic(pchar_aq, fpout);

  clock->time_spent[MAT_GET_AQ] += CHR_end_timer();
  // LP_printf(fpout,"erreur max for lake : %e\n",pic_max_wet);
  return pic_max_wet;
}

double WET_calc_pic(s_carac_wet *pcarac_wet, FILE *fpout) {
  double pic_wet_max, pic_wet_temp;
  int nw, nwet;
  s_lake_wet *pwet;
  nwet = pcarac_wet->nwet;
  pic_wet_max = 0.0;
  pic_wet_temp = (double)-CODE_WET;

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    pic_wet_temp = pwet->phydro->h[T_WET] - pwet->phydro->h[ITER_WET];
    pic_wet_max = WET_max(pic_wet_max, pic_wet_temp);
  }
  return pic_wet_max;
}

void WET_solve_diffusiv(s_carac_aq *pchar_aq, s_carac_wet *pcarac_wet, int nday, int calc_state, s_chronos_CHR *chronos, s_out_io *poutaq, s_clock_CHR *clock, s_out_io **pout, FILE *fpout) {

  int err, i;
  double drawd;
  double t;
  s_gc *pgc;
  s_cmsh *pcmsh;
  s_param_aq *param;
  i = 0;
  drawd = 0;

  // err=YES;
  pgc = pchar_aq->calc_aq;
  pcmsh = pchar_aq->pcmsh;
  param = pchar_aq->settings;
  // CHR_refresh_t(chronos);
  CHR_begin_timer(); // LV 3/09/2012

  AQ_calc_vector_b(pchar_aq, chronos, chronos->t[CUR_CHR], fpout);
  clock->time_spent[MAT_FILL_AQ] += CHR_end_timer();

  // if(param->aq_type==UNCONFINED)
  //	{

  // BL faire fonction solve_unconfined
  //  BL pour cela boucle de picard FAIT OK
  // BL  ajout de la fonction recalc_Transm FAIT A VERIF
  // BL  dans mat6 faudrait il mettre une condition sur le layer unconfined (sorte de calc_mat6 unconfined) pour ne pas tout recalculer tout le reste devrait etre identique le solve confined est juste GC_solve_gc!! A FAIRE sans doute le plus chiant...
  //}
  CHR_begin_timer(); // LV 3/09/2012

  // NG : 11/06/2021 : GC_configure_gc() added to update GC parameters according to the time-step or iteration we're at.
  GC_configure_gc(pgc, &nday, fpout);
  GC_solve_gc(pgc, AQ_GC, fpout);

  clock->time_spent[MAT_SOLVE_AQ] += CHR_end_timer();
  // system("rm /home/blabarthe/Programmes/cawaqs/trunk/Cmd_files/mac5*");
  CHR_begin_timer(); // LV 3/09/2012
  drawd = AQ_calc_drawd(pchar_aq, fpout);
  // LP_printf(fpout,"rabattement max : %e\n",drawd);

  AQ_refresh_H_interpol(pchar_aq, chronos, poutaq, fpout);
  WET_refresh_ele_h_wet_all(pcarac_wet, fpout);
  WET_refresh_h_lake(pcarac_wet, nday, chronos->dt, calc_state, fpout);
  clock->time_spent[MAT_GET_AQ] += CHR_end_timer();
  return;
}
