/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_geom.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"
#include "GC.h"
#include "AQ.h"
#include "WET.h"

/**\fn s_lake_wet *WET_ini_lake()
 * \brief initialiser une structure lake et qui retourne un pointer vers cette structure
 *  return pwet
 */
s_geom_wet *WET_ini_geom(void) {
  s_geom_wet *pgeom;

  pgeom = new_geom_wet();
  bzero((char *)pgeom, sizeof(s_geom_wet));

  return pgeom;
}

/* \fn s_geom_wet *WET_chain_lake_bck(s_geom_wet *pgeom1,s_geom_wet *pgeom2)
 * \brief Links two graviers with structure s_geom_wet
 *
 * retrun pgeom1
 */
s_geom_wet *WET_chain_geom_bck(s_geom_wet *pgeom1, s_geom_wet *pgeom2) {
  pgeom1->next = pgeom2;
  pgeom2->prev = pgeom1;
  return pgeom1;
}

/** \fn s_geom_wet *WET_secured_chain_geom_bck(s_geom_wet *pgeom1,s_geom_wet *pgeom2)
 * \brief chain gravier
 *
 * return pgeom
 *
 */
s_geom_wet *WET_secured_chain_geom_bck(s_geom_wet *pgeom1, s_geom_wet *pgeom2) {
  if (pgeom1 != NULL)
    pgeom1 = WET_chain_geom_bck(pgeom1, pgeom2);
  else
    pgeom1 = pgeom2;
  return pgeom1;
}

/** \fn s_geom_wet *WET_chain_geom_fwd(s_geom_wet *pgeom1,s_geom_wet *pgeom2)
 * \brief Links two graviers with structure s_geom_wet
 *
 * retrun pgeom2
 */
s_geom_wet *WET_chain_geom_fwd(s_geom_wet *pgeom1, s_geom_wet *pgeom2) {
  pgeom1->next = pgeom2;
  pgeom2->prev = pgeom1;
  return pgeom2;
}

/** \fn s_geom_wet *WET_secured_chain_geom_fwd(s_geom_wet *pgeom1,s_geom_wet *pgeom2)
 * \brief chain gravier
 *
 * return pgeom
 *
 */
s_geom_wet *WET_secured_chain_geom_fwd(s_geom_wet *pgeom1, s_geom_wet *pgeom2) {
  if (pgeom1 != NULL)
    pgeom1 = WET_chain_geom_fwd(pgeom1, pgeom2);
  else
    pgeom1 = pgeom2;
  return pgeom1;
}

/**\fn s_geom_wet **WET_tab_geom(s_geom_wet *pele,int nwet,FILE *fpout)
 *\brief put geom in array.
 *
 * put pgeom->next in p_wet[i+1], chain pgeom is created before in input.y
 *\return p_wet
 */
s_geom_wet **WET_tab_geom(s_geom_wet *pgeom, int nwet, FILE *fpout) {

  int i;
  s_geom_wet **p_geom;
  s_geom_wet *ptmp;
  // fprintf(stdout,"done3\n"); // BL to debug
  ptmp = WET_browse_geom(pgeom, BEGINNING_WET);

  p_geom = (s_geom_wet **)malloc(nwet * sizeof(s_geom_wet *));
  // pgeom=WET_browse_geom(pgeom,BEGINNING_WET); //????
  // LP_printf(fpout,"\ngeom id : %d\n",pgeom->id_wet); BL to debug
  for (i = 0; i < nwet; i++) {
    // LP_printf(fpout,"ID_wet : %d\n",pgeom->id_wet); BL to debug
    p_geom[i] = ptmp;
    ptmp = ptmp->next;
  }
  return p_geom;
}

s_geom_wet **WET_ini_tab_geom(int ngeom, FILE *fpout) {

  int ng;
  s_geom_wet **p_geom;

  p_geom = (s_geom_wet **)malloc(ngeom * sizeof(s_geom_wet *));

  for (ng = 0; ng < ngeom; ng++) {

    p_geom[ng] = WET_ini_geom();
  }
  return p_geom;
}
/**\fn s_ele_lake_wet *MSH_browse_lake(s_ele_lake_wet *pelewet,int iwhere)
 *\brief rewind or forward chained elements
 *
 * iwhere can be BEGINNING_WET or END_WET
 */
s_geom_wet *WET_browse_geom(s_geom_wet *pgeom, int iwhere) {
  s_geom_wet *ptmp, *preturn;
  ptmp = pgeom;

  while (ptmp != NULL) {
    preturn = ptmp;

    // fprintf(stdout,"done4\n"); // BL to debug
    switch (iwhere) {
    case BEGINNING_WET:
      ptmp = ptmp->prev;
      break;
    case END_WET:
      ptmp = ptmp->next;
      break;
    }
  }
  return preturn;
}

/**\fn s_id_io **WET_get_list_geom(s_temp_wet **ptemp_wet,s_cmsh *pcmsh,int nwet,FILE *fp)
 *\brief get ngeom for every lake
 *
 * called in WET_define_geom(s_carac_wet *pcarac_wet, s_carac_aq *pcarac_aq,s_temp_wet **ptemp_wet,FILE *fp)
 */
s_id_io **WET_get_list_geom(s_temp_wet **ptemp_wet, s_cmsh *pcmsh, int nwet, FILE *fp) {

  s_id_io **n_geom1;
  s_id_io *ptmpgeom1, *ptmpgeom2;
  s_temp_wet *ptempwet;
  int nlayer, id_ele, flag;
  int nw;
  n_geom1 = (s_id_io **)malloc(nwet * sizeof(s_id_io *));

  for (nw = 0; nw < nwet; nw++) {
    ptempwet = WET_browse_temp(ptemp_wet[nw], BEGINNING_WET);
    nlayer = MSH_get_layer_rank_by_name(pcmsh, ptempwet->name_aq, fp);
    id_ele = ptempwet->id_ele;
    n_geom1[nw] = IO_create_id(nlayer, id_ele);
    ptmpgeom1 = n_geom1[nw];
    ptempwet = ptempwet->next;

    while (ptempwet != NULL) {
      nlayer = MSH_get_layer_rank_by_name(pcmsh, ptempwet->name_aq, fp);
      id_ele = ptempwet->id_ele;
      ptmpgeom2 = IO_create_id(nlayer, id_ele);

      while (ptmpgeom1 != NULL) {
        if (ptmpgeom2->id_lay == ptmpgeom1->id_lay) {

          flag = YES_WET;
          break;
        } else if (ptmpgeom2->id_lay != ptmpgeom1->id_lay) {
          flag = NO_WET;
          ptmpgeom1 = ptmpgeom1->next;
        }
      }
      if (!flag) {

        ptmpgeom1 = IO_browse_id(n_geom1[nw], END_IO);
        ptmpgeom1 = IO_chain_fwd_id(ptmpgeom1, ptmpgeom2);
        // LP_printf(fp,"layer = %d for id_wet = %d id_ele = %d \n",ptmpgeom1->id_lay,nw+1,ptmpgeom1->id);
      }

      ptmpgeom1 = IO_browse_id(ptmpgeom1, BEGINNING_IO);
      // LP_printf(fp,"ID_WET done3b_is!\n");
      // LP_printf(fp,"layer = %d for id_wet = %d id_ele = %d \n",ptmpgeom1->id_lay,nw+1,ptmpgeom1->id);
      ptempwet = ptempwet->next;
    }
    n_geom1[nw] = ptmpgeom1;
  }

  return n_geom1;
}

/**\fn void WET_define_geom(s_carac_wet *pcarac_wet, s_carac_aq *pcarac_aq,s_temp_wet **ptemp_wet,FILE *fp)
 *\brief to define geome in every lake and assign values to pcarac_wet->p_wet
 *
 * free s_temp_wet **ptemp_wet and s_id_io
 * called in input.y dans cawaqs
 */
void WET_define_geom(s_carac_wet *pcarac_wet, s_carac_aq *pcarac_aq, s_temp_wet **ptemp_wet, FILE *fp) {

  s_id_io **n_geom;
  s_id_io *ptempgeom;
  s_lake_wet *pwet;
  s_temp_wet *ptempwet;
  s_cmsh *pcmsh;
  int num_geom, ng, nw, nwet;
  int nele, nlayer, id_ele;
  int *e;
  nwet = pcarac_wet->nwet;
  pcmsh = pcarac_aq->pcmsh;
  // LP_printf(fp,"ID_WET done2!\n");
  n_geom = WET_get_list_geom(ptemp_wet, pcmsh, nwet, fp);
  for (nw = 0; nw < nwet; nw++) {
    ptempgeom = IO_browse_id(n_geom[nw], BEGINNING_IO);
    while (ptempgeom != NULL) {
      // LP_printf(fp,"id_layer = %d id_ele = %d\n",ptempgeom->id_lay,ptempgeom->id);
      ptempgeom = ptempgeom->next;
    }
  }
  // LP_printf(fp,"ID_WET done4!\n");
  for (nw = 0; nw < nwet; nw++) {
    num_geom = 0;
    nele = 0;
    pwet = pcarac_wet->p_wet[nw];
    pwet->id_wet = nw + 1;
    ptempgeom = IO_browse_id(n_geom[nw], BEGINNING_IO);
    ptempwet = WET_browse_temp(ptemp_wet[nw], BEGINNING_WET);
    while (ptempgeom != NULL) {
      num_geom++;
      ptempgeom = ptempgeom->next;
    }

    pwet->ngeom = num_geom;
    // LP_printf(fp,"ID_WET = %d has %d geom !\n", nw+1,pwet->ngeom);
    pwet->p_geom = WET_ini_tab_geom(num_geom, fp);
    while (ptempwet != NULL) {
      nlayer = MSH_get_layer_rank_by_name(pcmsh, ptempwet->name_aq, fp);
      strcpy(pwet->p_geom[nlayer]->name_aq, ptempwet->name_aq);
      pwet->p_geom[nlayer]->nele++;
      ptempwet = ptempwet->next;
    }

    e = (int *)malloc(pwet->ngeom * sizeof(int));
    bzero((char *)e, sizeof(int));
    for (ng = 0; ng < pwet->ngeom; ng++) {
      nele = pwet->p_geom[ng]->nele;
      pwet->p_geom[ng]->p_ele = (s_ele_msh **)malloc(nele * sizeof(s_ele_msh *));
      e[ng] = 0;
    }

    ptempwet = WET_browse_temp(ptemp_wet[nw], BEGINNING_WET);

    while (ptempwet != NULL) {

      nlayer = MSH_get_layer_rank_by_name(pcmsh, ptempwet->name_aq, fp);

      id_ele = ptempwet->id_ele;
      // LP_printf(fp,"ID_wet = %d ID_layer = %d id_ele = %d! e[nlayer] = %d\n",nw+1,nlayer,id_ele,e[nlayer]);
      pwet->p_geom[nlayer]->p_ele[e[nlayer]++] = MSH_get_ele(pcmsh, nlayer, id_ele, fp);
      ptempwet = ptempwet->next;
    }
    WET_free_temp_serie(ptemp_wet[nw], fp);
    IO_free_id_serie(n_geom[nw], fp);
  }

  return;
}

/**\fn void WET_calc_surf_wet(s_carac_wet *pcarac_wet,FILE *fp
 *\brief calculate surface of lake in top layer, for a lake rectangulaire
 *
 */
void WET_calc_surf_wet(s_carac_wet *pcarac_wet, FILE *fp) {
  int nwet, ngeom, nele;
  int nw, ng, e;

  double surf;

  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ele_msh *pele;
  nwet = pcarac_wet->nwet;

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    ngeom = pwet->ngeom;
    surf = 0.0;
    for (ng = 0; ng < ngeom; ng++) {
      pgeom = pwet->p_geom[ng];
      nele = pgeom->nele;
      for (e = 0; e < nele; e++) {
        pele = pgeom->p_ele[e];
        if (pele->loc == TOP_MSH)
          surf += pele->pdescr->surf;
      }
    }

    pwet->surf = surf;
    // LP_printf(fp,"id_wet = %d surf = %f\n",nw + 1,pwet->surf);
  }
  return;
}
