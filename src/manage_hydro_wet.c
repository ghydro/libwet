/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_hydro_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "libprint.h"

#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"
#include "GC.h"
#include "AQ.h"

#include "WET.h"

/**\fn s_lake_wet *WET_ini_hydrowet()
 * \brief initialiser une structure lake et qui retourne un pointer vers cette structure
 *  return pwet
 */
s_hydro_wet *WET_ini_hydrowet(void) {
  s_hydro_wet *phydrowet;

  phydrowet = new_hydro_wet();
  bzero((char *)phydrowet, sizeof(s_hydro_wet));

  return phydrowet;
}

void WET_ini_hini_wet(s_carac_aq *pcarac_aq, s_carac_wet *pcarac_wet, FILE *fp) {

  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ele_msh *pele;
  int nwet, ngeom, nele;
  int e, nw, ng;
  int nlayer, id_ele;

  nwet = pcarac_wet->nwet;

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    ngeom = pwet->ngeom;
    for (ng = 0; ng < ngeom; ng++) {
      pgeom = pwet->p_geom[ng];
      nele = pgeom->nele;
      for (e = 0; e < nele; e++) {
        pele = pgeom->p_ele[e];
        nlayer = pele->player->id - 1;
        id_ele = pele->id[INTERN_MSH];
        pele->phydro->h[ITER] = pwet->phydro->h[INI_WET];
        pele->phydro->h[T] = pwet->phydro->h[INI_WET];
        pele->phydro->h[INTERPOL] = pwet->phydro->h[INI_WET];
        pele->phydro->h[ITER_INTERPOL] = pwet->phydro->h[INI_WET];
        pele->phydro->h[PIC] = pwet->phydro->h[INI_WET];

        // LP_printf(fp,"nlayer = %d, id_ele = %d, hini = %f\n",nlayer,id_ele,pele->phydro->h[ITER_INTERPOL]);
      }
    }
  }
  return;
}
