/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_condu_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "libprint.h"

#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

#include "GC.h"
#include "AQ.h"
#include "WET.h"

s_condu_wet *WET_ini_condu(void) {
  s_condu_wet *pconduwet;

  pconduwet = new_condu_wet();
  bzero((char *)pconduwet, sizeof(s_condu_wet));
  pconduwet->id_ele = 0;
  // pconduwet->condu[V_WET] = 0.0;
  pconduwet->condu[H_WET_B] = 0.0; // TV 25/09/2019
  pconduwet->condu[V_WET_N] = 0.0; // TV 25/09/2019
  pconduwet->condu[V_WET_E] = 0.0; // TV 25/09/2019
  pconduwet->condu[V_WET_S] = 0.0; // TV 25/09/2019
  pconduwet->condu[V_WET_W] = 0.0; // TV 25/09/2019

  return pconduwet;
}

/*
 *
 *nele_wet : number of elements in a lake
 */
s_condu_wet **WET_ini_tab_condu(int nele_wet, FILE *fpout) {

  int i;
  s_condu_wet **pcondu_wet;

  pcondu_wet = (s_condu_wet **)malloc(nele_wet * sizeof(s_condu_wet *));

  for (i = 0; i < nele_wet; i++) {

    pcondu_wet[i] = WET_ini_condu();
  }
  return pcondu_wet;
}

/*
 *modify conductances for lake faces
 *
 */
// void WET_define_condu_wet(s_cmsh *pcmsh, s_carac_wet *pcarac_wet,FILE *fpout)
void WET_define_condu_wet(s_cmsh *pcmsh, s_carac_wet *pcarac_wet, s_chronos_CHR *chronos, FILE *fpout) // TV 17/12/2019
{
  int idir, itype;
  int nw, ng, e;
  int nwet = 0, ngeom, nele;
  int nele_wet, m;
  int id_ele, id_layer;
  int type_mean;      // TV 17/12/2019
  double alpha_z_wet; // TV 11/02/2020
  double cond[NTYPE_FACE_WET], conductance, surf, dx;
  double l_neigh_1, l_neigh_2, T_neigh_1, T_neigh_2, Cs; // TV 17/12/2019
  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_condu_wet *pconduwet;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;

  nwet = pcarac_wet->nwet;

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    ngeom = pwet->ngeom;
    nele_wet = 0;

    for (ng = 0; ng < ngeom; ng++) {
      pgeom = pwet->p_geom[ng];
      nele_wet += pgeom->nele; // get number of elements in a lake (for all geoms)
    }

    for (e = 0; e < nele_wet; e++) // modify every element of lake
    {
      pconduwet = pwet->phydro->pcondu_wet[e]; // structure conductance pour lacs
      // cond[H_WET] = pconduwet->condu[H_WET_B]; //conductances for an element of lake s-1
      id_ele = pconduwet->id_ele;
      id_layer = MSH_get_layer_rank_by_name(pcmsh, pconduwet->name_aq, fpout);
      pele = MSH_get_ele(pcmsh, id_layer, id_ele, fpout);
      type_mean = pcmsh->p_layer[id_layer]->mean_type; // TV 17/12/2019

      for (idir = X_MSH; idir < ND_MSH; idir++) {
        for (itype = ONE; itype < NFACE_MSH; itype++) {
          pface = pele->face[idir][itype]; // BL ATTENTION aux subfaces !! tu ne vérifies pas si la dimention de ton élément lac est de même dimension que l'élément d'à côté. si la dimension de ton lac est supérieure à la dimension de la maille d'à côté ça devrait planter
          if (idir != Z_MSH)               // i.e. vertical faces along X and Y
          {
            if (pface->p_subface != NULL) {
              for (m = 0; m < Z_MSH; m++) // loop over vertical subsurfaces (along X and Y)
              {
                psubf = pface->p_subface[m];
                if (psubf->type != BOUND) // BL IL faut aussi vérifier que ta face n'est pas en bordure de modèle
                {
                  surf = psubf->phydro->Thick * pele->pdescr->l_side;                                 // surface of pface
                                                                                                      // psubf->phydro->ptransm->transm[T_HOMO] = cond[V_WET]*surf; //s-1 to m2/s
                  psubf->phydro->ptransm->transm[T_HOMO] = pconduwet->condu[2 * idir + itype] * surf; // s-1 to m2/s //TV 25/09/2019 ATTENTION VERIFIER CE CAS!!
                  dx = TS_min(psubf->p_descr[ONE]->l_side, psubf->p_descr[TWO]->l_side);
                  psubf->phydro->pcond->conductance = psubf->phydro->ptransm->transm[T_HOMO] * 2 * dx / (psubf->p_descr[ONE]->l_side + psubf->p_descr[TWO]->l_side);
                }
              }
            } else // if no subsurfaces
            {
              surf = pface->phydro->Thick * pele->pdescr->l_side; // surface of pface par mètre de l'epaisseur

              /*
              //Former Version
              //pface->phydro->ptransm->transm[T_HOMO] = cond[V_WET]*surf; //s-1 to m2/s Origin version one value for all the faces A supprimer!!
              pface->phydro->ptransm->transm[T_HOMO] = pconduwet->condu[2*idir+itype]*surf; //TV 25/09/2019 ca marche avec le calcul de N,E,S ou W avec idir et itype.
              dx=TS_min(pface->p_descr[ONE]->l_side,pface->p_descr[TWO]->l_side); //former version
              pface->phydro->pcond->conductance = pface->phydro->ptransm->transm[T_HOMO]*2*dx/(pface->p_descr[ONE]->l_side+pface->p_descr[TWO]->l_side); // Former version
              // End Former Version
              */

              // New Version TV 02/2020

              // TV 17/12/2019: Calcul de la conductance equivalente Ceq [m2.s-1] (anciennement prescrite dans DATA_WET):
              //                Si on donne la conductance specifique (Kb/b=Cg) [s-1] en entree du calcul de la conductance equivalente,

              if (itype == ONE) {
                // pface->p_ele[TWO]->phydro->ptransm->transm[T_HOMO] = pconduwet->condu[2*idir+itype]*surf; //[m2.s-1] //TV 17/12/2019 OK
                l_neigh_1 = pface->p_descr[ONE]->l_side; // TV 17/12/2019
                l_neigh_2 = 1.0;                         // TV 17/12/2019
                // T_neigh_1 = pface-> phydro->ptransm->transm[T_HOMO]; //TV 03/02/2020 //[m2.s-1]
                T_neigh_1 = pface->phydro->ptransm->transm[T_HOMO] / pface->phydro->Thick; // TV 03/02/2020 [s-1]
                T_neigh_2 = pconduwet->condu[2 * idir + itype];                            // 03/02/2020 [s-1]
              }
              if (itype == TWO) {
                // pface->p_ele[ONE]->phydro->ptransm->transm[T_HOMO] = pconduwet->condu[2*idir+itype]*surf; //[m2.s-1] //TV 17/12/2019 OK
                l_neigh_1 = 1.0;                                // TV 17/12/2019
                l_neigh_2 = pface->p_descr[TWO]->l_side;        // TV 17/12/2019
                T_neigh_1 = pconduwet->condu[2 * idir + itype]; // TV 03/02/2020 [s-1]
                // T_neigh_2 = pface-> phydro->ptransm->transm[T_HOMO]; //TV 03/02/2020 //[m2.s-1]
                T_neigh_2 = pface->phydro->ptransm->transm[T_HOMO] / pface->phydro->Thick; // TV 03/02/2020 [s-1]
              }

              // T_neigh_1 = pface->p_ele[ONE]->phydro->ptransm->transm[T_HOMO]; //TV 17/12/2019 commenté le 03/02/2020
              // T_neigh_2 = pface->p_ele[TWO]->phydro->ptransm->transm[T_HOMO]; //TV 17/12/2019 commenté le 03/02/2020

              Cs = AQ_calc_Tmean(T_neigh_1, l_neigh_1, T_neigh_2, l_neigh_2, type_mean, fpout); // TV 17/12/2019
              // pface->phydro->pcond->conductance = Cs*surf/(l_neigh_1+l_neigh_2); // [m2.s-1] TV 17/12/2019 with AQ_calc_Tmean
              pface->phydro->pcond->conductance = surf * T_neigh_1 * T_neigh_2 / (l_neigh_1 * T_neigh_2 + l_neigh_2 * T_neigh_1); // TV 17/12/2019 New version
              pface->phydro->ptransm->transm[T_HOMO] = pface->phydro->pcond->conductance;                                         //[m2.s-1] //02/02/2020
              // End New version TV 02/2020
            }
          }

          else if (itype == ONE) // BL il suffit de modifier la conductance dans la direction Z ONE face bas de la maille
          {
            if (pface->phydro->pcond == NULL && (pface->loc == BOT_MSH)) {
              LP_error(fpout, "libwet%4.2f %s in %s l%d: the conductance of ele GIS %d ABS %d INTERN %d,on face in dir %s[%s] located at %s of the model, is not initialized\n", VERSION_WET, __func__, __FILE__, __LINE__, pele->id[GIS_MSH], pele->id[ABS_MSH], pele->id[INTERN_MSH], MSH_name_direction(idir, fpout), MSH_name_type(itype, fpout), MSH_name_position_cell(pface->loc, fpout));
            } else {

              // New version 02/2020
              // On est dans le cas du fond de la couche (itype=ONE):
              surf = pele->face[X_MSH][ONE]->p_descr[TWO]->l_side * 2 * pele->face[Y_MSH][ONE]->p_descr[TWO]->l_side * 2;      // surface du fond de la maille l(X)*l(Y)
              l_neigh_1 = pface->p_ele[ONE]->phydro->Thick * 0.5;                                                              // TV 17/12/2019 (couche craie)
              l_neigh_2 = 1.0;                                                                                                 // TV 17/12/2019 (graviere)
              alpha_z_wet = ALPHA_Z_DEFAULT_AQ;                                                                                // TV 11/02/2020
              alpha_z_wet = pcarac_wet->alpha_z_wet;                                                                           // 11/02/2020
              T_neigh_1 = pface->p_ele[ONE]->phydro->ptransm->transm[T_HOMO] * alpha_z_wet / pface->p_ele[ONE]->phydro->Thick; // [s-1] TV 17/12/2019 (couche craie K=T/e)
              T_neigh_2 = pconduwet->condu[H_WET_B];

              pface->phydro->pcond->conductance = surf * T_neigh_1 * T_neigh_2 / (l_neigh_1 * T_neigh_2 + l_neigh_2 * T_neigh_1); //[m2.s-1] TV 17/12/2019 New version
                                                                                                                                  // New version 02/2020

              // pface->phydro->pcond->conductance = pconduwet->condu[H_WET_B]*pele->pdescr->surf;//s-1 to m2/s //Former version
            }
          }
        }
      }
    }
  }
  return;
}

double *WET_calc_condu_sum_wet(s_lake_wet *pwet, FILE *fpout) {
  double *sum_condu_aq_wet;
  int *nface;
  int nf_V = 0, nf_H;
  s_face_msh **p_faces_V;
  s_face_msh **p_faces_H;
  s_ele_msh **pneigh_ele_V;
  s_ele_msh **pneigh_ele_H;

  nface = pwet->nface;
  p_faces_V = pwet->p_faces_V;
  p_faces_H = pwet->p_faces_H;
  pneigh_ele_V = pwet->pneigh_ele_V;
  pneigh_ele_H = pwet->pneigh_ele_H;
  sum_condu_aq_wet = (double *)malloc(TYPE_SUM_WET * sizeof(double));
  sum_condu_aq_wet[SUM_CONDU] = 0.0;
  sum_condu_aq_wet[SUM_PROD_CONDU] = 0.0;

  //   for(nf_V = 0; nf_V < nface[V_WET]; nf_V++)
  for (nf_V = 0; nf_V < nface[V_WET_W]; nf_V++) {
    sum_condu_aq_wet[SUM_CONDU] += p_faces_V[nf_V]->phydro->pcond->conductance;
    sum_condu_aq_wet[SUM_PROD_CONDU] += p_faces_V[nf_V]->phydro->pcond->conductance * pneigh_ele_V[nf_V]->phydro->h[T]; // 25-05-2016
  }
  for (nf_H = 0; nf_H < nface[H_WET]; nf_H++) {
    sum_condu_aq_wet[SUM_CONDU] += p_faces_H[nf_H]->phydro->pcond->conductance;
    sum_condu_aq_wet[SUM_PROD_CONDU] += p_faces_H[nf_H]->phydro->pcond->conductance * pneigh_ele_H[nf_H]->phydro->h[T]; // 21-06-2016
  }
  return sum_condu_aq_wet;
}
