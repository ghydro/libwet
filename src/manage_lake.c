/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_lake.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "libprint.h"

#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

#include "GC.h"
#include "AQ.h"
#include "WET.h"

/**\fn s_lake_wet *WET_ini_lake()
 * \brief initialiser une structure lake et qui retourne un pointer vers cette structure
 *  return pwet
 */
s_lake_wet *WET_ini_lake(void) {
  s_lake_wet *pwet;

  pwet = new_lake_wet();
  bzero((char *)pwet, sizeof(s_lake_wet));

  pwet->phydro = WET_ini_hydrowet();

  return pwet;
}

/* \fn s_lake_wet *WET_chain_lake_bck(s_lake_wet *pwet1,s_lake_wet *pwet2)
 * \brief Links two graviers with structure s_lake_wet
 *
 * retrun pwet1
 */
s_lake_wet *WET_chain_lake_bck(s_lake_wet *pwet1, s_lake_wet *pwet2) {
  pwet1->next = pwet2;
  pwet2->prev = pwet1;
  return pwet1;
}

/** \fn s_lake_wet *WET_secured_chain_lake_bck(s_lake_wet *pwet1,s_lake_wet *pwet2)
 * \brief chain gravier
 *
 * return pwet
 *
 */
s_lake_wet *WET_secured_chain_lake_bck(s_lake_wet *pwet1, s_lake_wet *pwet2) {
  if (pwet1 != NULL)
    pwet1 = WET_chain_lake_bck(pwet1, pwet2);
  else
    pwet1 = pwet2;
  return pwet1;
}

/** \fn s_lake_wet *WET_chain_lake_fwd(s_lake_wet *pwet1,s_lake_wet *pwet2)
 * \brief Links two graviers with structure s_lake_wet
 *
 * retrun pwet2
 */
s_lake_wet *WET_chain_lake_fwd(s_lake_wet *pwet1, s_lake_wet *pwet2) {
  pwet1->next = pwet2;
  pwet2->prev = pwet1;
  return pwet2;
}

/** \fn s_lake_wet *WET_secured_chain_lake_fwd(s_lake_wet *pwet1,s_lake_wet *pwet2)
 * \brief chain gravier
 *
 * return pwet
 *
 */
s_lake_wet *WET_secured_chain_lake_fwd(s_lake_wet *pwet1, s_lake_wet *pwet2) {
  if (pwet1 != NULL)
    pwet1 = WET_chain_lake_fwd(pwet1, pwet2);
  else
    pwet1 = pwet2;
  return pwet1;
}

/**\fn s_ele_lake_wet *MSH_browse_lake(s_ele_lake_wet *pelewet,int iwhere)
 *\brief rewind or forward chained elements
 *
 * iwhere can be BEGINNING_WET or END_WET
 */
s_lake_wet *WET_browse_lake(s_lake_wet *pwet, int iwhere) {
  s_lake_wet *ptmp, *preturn;
  ptmp = pwet;

  while (ptmp != NULL) {
    preturn = ptmp;

    // fprintf(stdout,"done4\n"); // BL to debug
    switch (iwhere) {
    case BEGINNING_WET:
      ptmp = ptmp->prev;
      break;
    case END_WET:
      ptmp = ptmp->next;
      break;
    }
  }
  return preturn;
}

/**\fn s_lake_wet **WET_tab_lake(s_lake_wet *pele,int nwet,FILE *fpout)
 *\brief put lake in array.
 *
 * put pwet->next in p_wet[i+1], chain pwet is created before in input.y
 *\return p_wet
 */
s_lake_wet **WET_tab_lake(s_lake_wet *pwet, int nwet, FILE *fpout) {

  int i;
  s_lake_wet **p_wet;
  s_lake_wet *ptmp;
  ptmp = WET_browse_lake(pwet, BEGINNING_WET);

  p_wet = (s_lake_wet **)malloc(nwet * sizeof(s_lake_wet *));
  for (i = 0; i < nwet; i++) {
    p_wet[i] = ptmp;
    ptmp = ptmp->next;
  }
  return p_wet;
}
