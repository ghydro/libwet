/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_carac.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "libprint.h"

#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
// #include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"

#include "WET.h"

/**\fn s_carac_wet *WET_create_carac_lake()
 * \brief create and initialize a carac_wet structure
 *  return pwet
 */
s_carac_wet *WET_create_carac_lake() {
  s_carac_wet *pcarac;

  pcarac = new_carac_wet();
  bzero((char *)pcarac, sizeof(s_carac_wet));

  pcarac->eps_wet = EPS_DEFAULT_WET;
  pcarac->nit_wet = NIT_DEFAULT_WET;

  return pcarac;
}

/**\fn s_lake_wet **WET_ini_tab_lake(int nwet)
 * \brief initialiser une table structure lake et qui retourne un pointer vers cette structure
 *  return pwet
 */
s_lake_wet **WET_ini_tab_lake(int nwet, FILE *fpout) {
  int i;
  s_lake_wet **p_wet;
  p_wet = (s_lake_wet **)malloc(nwet * sizeof(s_lake_wet *));

  for (i = 0; i < nwet; i++) {
    p_wet[i] = WET_ini_lake();
  }
  return p_wet;
}

/**\fn void WET_print_carac_wet(s_carac_wet *pcarac_wet,FILE *fp)
 * \brief print caracteristic of lakes; summary for debug
 *
 */
void WET_print_carac_wet(s_carac_wet *pcarac_wet, FILE *fp) {
  /*print caracteristic of lakes; summary for debug*/
  int nw, ng, e;
  int nwet, ngeom, nele;
  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ele_msh *pele;

  nwet = pcarac_wet->nwet;
  LP_printf(fp, "Number of lakes is %d\n", nwet);
  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    LP_printf(fp, "Identifiant of lake is %d\n", pwet->id_wet);
    LP_printf(fp, "Initial water head is %f\n", pwet->phydro->h[INI_WET]);
    LP_printf(fp, "Precipitation is %.10f\t Evaporation is %.10f\n", pwet->phydro->q[WET_PRE], pwet->phydro->q[WET_EVAP]);
    LP_printf(fp, "Number of geom = %d\n", pwet->ngeom);
    ngeom = pwet->ngeom;
    for (ng = 0; ng < ngeom; ng++) {
      pgeom = pwet->p_geom[ng];
      nele = pgeom->nele;
      LP_printf(fp, "Layer name %s\n", pgeom->name_aq);
      for (e = 0; e < nele; e++) {
        pele = pgeom->p_ele[e];
        LP_printf(fp, "id_ele = %d\n", pele->id[INTERN_MSH]);
      }
    }
  }
}
