/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: struct_wet.h
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**********************************************************************/
/*                       LibWet library structs                      */
/**********************************************************************/

typedef struct lake_wet s_lake_wet;
typedef struct geom_wet s_geom_wet;
typedef struct hydro_wet s_hydro_wet;
typedef struct carac_wet s_carac_wet;
typedef struct temp_wet s_temp_wet;
typedef struct condu_wet s_condu_wet; //????????
typedef struct masse_blance_wet s_mb_wet;
typedef struct mto_wet s_mto_wet;

struct lake_wet {
  /*identifiant of lakes*/
  int id_wet;
  /*Number of intersected aquifer layers*/
  int ngeom;
  /* lake surface */
  double surf;
  /*Volume of lake*/
  double vol;
  /*number of faces lake-aquifer, vertical or horizontal V_WET H_WET*/
  int *nface;
  /*Pointers towards faces lake-aquifer*/
  s_face_msh **p_faces_V;
  s_face_msh **p_faces_H;
  /*Pointers towards neighbour aquifer elements*/
  s_ele_msh **pneigh_ele_V;
  s_ele_msh **pneigh_ele_H; // 25-05-2016
  /*Pointers towards geometric features allowing fora lake to intersect multiple aquifer layers*/
  s_geom_wet **p_geom;
  /*Pointer towards hydrological variables (level, fluxes)*/
  s_hydro_wet *phydro;
  /*Pointer towards climate data T H V*/
  s_mto_wet *pmto;
  /*pointer to next lake*/
  s_lake_wet *next;
  /*pointer to previous lake*/
  s_lake_wet *prev;
};

struct hydro_wet {
  /*Lake water level*/
  double h[NTIME_WET]; // Create the WET_indexes
  /*terms used in water balance, WET_PRE, WET_ETR, WET_Q etc.*/
  double q[WET_NTYPE];
  /*Elevation max for a lake*/
  double Z_max;
  /*Elevation min for a lake*/
  double Z_min;
  /*struct conductance for all elements in a lake*/
  s_condu_wet **pcondu_wet;
};

struct geom_wet {
  /*Name of the aquifer layer*/
  char name_aq[STRING_LENGTH_LP];
  /*number of elements  in a lake intersecting the aquifer layer*/
  int nele;
  /* geom lake surface, usefull for calculate h */
  // double surf;
  /*Thicknisse of geom*/
  // double Thick;
  /*aquifer top and bottom of geom*/
  // double Z[ABT_WET];
  /*Volume cumule of lake for geom*/
  // double V[VBT_WET];
  /*two table pointers to all elements in a lake intersecting the aquifer layer*/
  s_ele_msh **p_ele;
  /*pointer to next geom*/
  s_geom_wet *next;
  /*pointer to previous geom*/
  s_geom_wet *prev;
};

struct carac_wet {
  /*number of lakes*/
  int nwet;
  /*number of ietration-newthon 100 default*/
  int nit_wet;
  /*eps  for iteration newton*/
  double eps_wet;
  /*theta for schema semi-implict*/
  double theta;
  /*Vertical anisotropy factor*/
  double alpha_z_wet;
  /*length number of climate data*/
  int length_mto;
  FILE *fpmto;
  /*array of pointers towards all lakes*/
  s_lake_wet **p_wet;
  int read_evap; /*!< in [YES,NO] */ // TV 23/01/2020
};

struct temp_wet {

  /*Name of the aquifer layer*/
  char name_aq[STRING_LENGTH_LP];
  /*identifiant of lake*/
  int id_wet;
  /*identifiant of element*/
  int id_ele;
  /*pointer to next temp*/
  s_temp_wet *next;
  /*pointer to previous temp*/
  s_temp_wet *prev;
};

struct condu_wet {
  /*identifiant of element*/
  int id_ele;
  /*Name of the aquifer layer*/
  char name_aq[STRING_LENGTH_LP];
  /*conductance value for faces X or Z*/
  // double condu[ND_WET];
  // double condu[NTYPE_FACE_WET];
  double condu[NTYPE_FACE_COND_WET];
};

struct mto_wet {
  /*Pointer towards climate data T H V*/
  double **pftmto;
};

#define new_lake_wet() ((s_lake_wet *)malloc(sizeof(s_lake_wet)))
#define new_carac_wet() ((s_carac_wet *)malloc(sizeof(s_carac_wet)))
#define new_geom_wet() ((s_geom_wet *)malloc(sizeof(s_geom_wet)))
#define new_hydro_wet() ((s_hydro_wet *)malloc(sizeof(s_hydro_wet)))
#define new_temp_wet() ((s_temp_wet *)malloc(sizeof(s_temp_wet)))
#define new_condu_wet() ((s_condu_wet *)malloc(sizeof(s_condu_wet)))
#define new_mto_wet() ((s_mto_wet *)malloc(sizeof(s_mto_wet)))