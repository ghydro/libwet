/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_mb_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
// #include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"

s_face_msh **WET_ini_pfaces_wet(int nface, FILE *fpout) {
  int nf; // 25-05-2016
  s_face_msh **p_faces;

  p_faces = (s_face_msh **)malloc(nface * sizeof(s_face_msh *));

  for (nf = 0; nf < nface; nf++)
    p_faces[nf] = new_face_msh();

  return p_faces;
}
s_ele_msh **WET_ini_peles_wet(int nele, FILE *fpout) // 25-05-2016
{
  int ne;
  s_ele_msh **p_ele;

  p_ele = (s_ele_msh **)malloc(nele * sizeof(s_ele_msh *));
  for (ne = 0; ne < nele; ne++)
    p_ele[ne] = new_ele_msh();

  return p_ele; // 25-05-2016
}
void WET_calc_mb_face_wet(s_carac_aq *pchar_aq, s_lake_wet *pwet, s_chronos_CHR *chronos, s_out_io **pout_all, FILE *fpout) {
  int ngeom, nele;
  int ng, e;
  int a0;
  s_geom_wet *pgeom;
  s_ele_msh *pele;

  double deltat;
  s_out_io *pout;
  double t_out, dt, t;
  double theta;
  s_cmsh *pcmsh;

  theta = pchar_aq->settings->general_param[THETA_AQ];
  a0 = pchar_aq->settings->a0;
  pcmsh = pchar_aq->pcmsh;

  // if(pout_all[MASS_IO]!=NULL || pout_all[FLUX_IO]!=NULL)//FB 06/03/18 FLUX_IO does not exist anymore
  if (pout_all[MASS_IO] != NULL) // FB 06/03/18 FLUX_IO does not exist anymore
  {
    // if(pout_all[FLUX_IO]!=NULL)//FB 06/03/18 FLUX_IO does not exist anymore
    //	  pout=pout_all[FLUX_IO];//FB 06/03/18 FLUX_IO does not exist anymore
    // else                       //FB 06/03/18 FLUX_IO does not exist anymore
    pout = pout_all[MASS_IO];

    t = chronos->t[CUR_CHR];
    dt = chronos->dt;
    t_out = pout->t_out[CUR_IO];

    ngeom = pwet->ngeom;
    for (ng = 0; ng < ngeom; ng++) {
      pgeom = pwet->p_geom[ng];
      nele = pgeom->nele;
      for (e = 0; e < nele; e++) {
        pele = pgeom->p_ele[e];
        if (t_out <= t) {
          if (t_out < t && t_out > (t - dt)) {
            deltat = t - t_out;
            LP_printf(fpout, "In libwet%4.2f %s in %s l.%d The mass balance calculation needs to be programmed for lakes\n", VERSION_WET, __func__, __FILE__, __LINE__);
            //		   AQ_calc_mb_face(pcmsh,pele,deltat,t_out,theta,a0,fpout);//NF 22/2/2018 This function does not exist anymore following FB debugging. Write a new one, or check the one you need in libaq
          }

          else
            LP_printf(fpout, "In libwet%4.2f %s in %s l.%d The mass balance calculation needs to be programmed for lakes\n", VERSION_WET, __func__, __FILE__, __LINE__);
          // AQ_calc_mb_face(pcmsh,pele,dt,t,theta,a0,fpout);//NF 22/2/2018 This function does not exist anymore following FB debugging. Write a new one, or check the one you need in libaq

        } else // si t_out > t on calcule les mb pour addition
        {
          LP_printf(fpout, "In libwet%4.2f %s in %s l.%d The mass balance calculation needs to be programmed for lakes\n", VERSION_WET, __func__, __FILE__, __LINE__);
          // AQ_calc_mb_face(pcmsh,pele,dt,t,theta,a0,fpout);//NF 22/2/2018 This function does not exist anymore following FB debugging. Write a new one, or check the one you need in libaq
        }
      }
    }
  }
  return;
}

void WET_reinit_mb_face_wet(s_lake_wet *pwet, FILE *fpout) {
  int ngeom, nele;
  int ng, e;
  s_geom_wet *pgeom;
  s_ele_msh *pele;

  ngeom = pwet->ngeom;
  for (ng = 0; ng < ngeom; ng++) {
    pgeom = pwet->p_geom[ng];
    nele = pgeom->nele;
    for (e = 0; e < nele; e++) {
      pele = pgeom->p_ele[e];
      // AQ_reinit_mb_face(pele->phydro->pmb,fpout);//NF 22/2/2018 This function does not exist anymore following FB debugging. Write a new one, or check the one you need in libaq
      LP_printf(fpout, "In libwet%4.2f %s in %s l.%d mass balance needs to be initialized\n", VERSION_WET, __func__, __FILE__, __LINE__);
    }
  }
  return;
}
void WET_refresh_ele_h_wet(s_lake_wet *pwet, FILE *fpout) {
  int ngeom, nele;
  int ng, e;
  s_geom_wet *pgeom;
  s_ele_msh *pele;

  ngeom = pwet->ngeom;

  for (ng = 0; ng < ngeom; ng++) {
    pgeom = pwet->p_geom[ng];
    nele = pgeom->nele;
    for (e = 0; e < nele; e++) {
      pele = pgeom->p_ele[e];
      pele->phydro->h[ITER] = pwet->phydro->h[ITER_WET];
      pele->phydro->h[ITER_INTERPOL] = pwet->phydro->h[ITER_WET];
    }
  }
  return;
}
void WET_refresh_ele_h_wet_all(s_carac_wet *pcarac_wet, FILE *fpout) // BL SW 05/07/2016
{

  int nw, nwet;
  s_lake_wet *pwet;

  nwet = pcarac_wet->nwet;
  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    WET_refresh_ele_h_wet(pwet, fpout);
  }
  return;
}

// ajoute p_faces dans la structure s_lake_wet et nface;
//  modifier condu comme condu[TYPE_FACE] vertical et horizontal

int *WET_face_aq_wet(s_lake_wet *pwet, FILE *fpout) {
  int ngeom, nele;
  int ng, e, yes;
  int *nface;
  int idir, itype;
  int id_ele, id_ele1;

  s_geom_wet *pgeom;
  s_ele_msh *pele;
  s_ele_msh **p_ele;
  s_face_msh *pface;
  ngeom = pwet->ngeom;
  nface = (int *)malloc(NTYPE_FACE_WET * sizeof(int));
  nface[V_WET] = 0;
  // nface[V_WET_N] = 0; //TV2019
  // nface[V_WET_E] = 0; //TV2019
  // nface[V_WET_S] = 0; //TV2019
  // nface[V_WET_W] = 0; //TV2019
  nface[H_WET] = 0;
  for (ng = 0; ng < ngeom; ng++) {
    pgeom = pwet->p_geom[ng];
    nele = pgeom->nele;
    for (e = 0; e < nele; e++) {
      pele = pgeom->p_ele[e];
      id_ele1 = pele->id[INTERN_MSH];
      // LP_printf(fpout,"id_ele1 = %d\n",id_ele1);
      for (idir = X_MSH; idir < ND_MSH; idir++) {
        for (itype = ONE; itype < NFACE_MSH; itype++) {
          pface = pele->face[idir][itype];
          if (idir != Z_MSH) {
            p_ele = pface->p_ele;
            // id_ele = p_ele[0]->id[INTERN_MSH] == id_ele ? p_ele[1]->id[INTERN_MSH] : id_ele1
            if (p_ele[0]->id[INTERN_MSH] == id_ele1)
              id_ele = p_ele[1]->id[INTERN_MSH];
            else
              id_ele = p_ele[0]->id[INTERN_MSH];
            // LP_printf(fpout,"id_ele = %d\n",id_ele);
            yes = WET_check_face_aq_wet(pgeom, id_ele, fpout);
            nface[V_WET] += yes;
            // nface[V_WET_N] += yes; //TV2019 attention le V_WET_N est temporaire!
          } else if (itype == ONE && ng == (ngeom - 1))
            nface[H_WET]++;
        }
      }
    }
  }
  LP_printf(fpout, "The number of aq-lake vertical faces is %d and horizontal faces is %d for lake %d\n", nface[V_WET], nface[H_WET], pwet->id_wet);
  // LP_error(fpout,"noramle\n");
  return nface;
}

void WET_define_face_ele_aq_wet(s_carac_aq *pcarac_aq, s_carac_wet *pcarac_wet, FILE *fpout) // 25-05-2016
{
  int nwet, ngeom, nele;
  int nw, ng, e;
  int *nface;
  int idface_V = 0, idele_V = 0; // 25-05-2016
  int idface_H = 0, idele_H = 0;
  int idir, itype;
  int id_ele, id_ele1, nlayer;

  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ele_msh *pele;
  s_ele_msh **p_ele;
  s_face_msh *pface;
  s_cmsh *pcmsh; // 25-05-2016

  nwet = pcarac_wet->nwet;
  pcmsh = pcarac_aq->pcmsh;
  for (nw = 0; nw < nwet; nw++) {
    idface_V = 0;
    idele_V = 0;
    idface_H = 0;
    idele_H = 0;
    pwet = pcarac_wet->p_wet[nw];
    nface = WET_face_aq_wet(pwet, fpout);
    pwet->nface = nface;

    pwet->p_faces_V = WET_ini_pfaces_wet(nface[V_WET], fpout);
    pwet->p_faces_H = WET_ini_pfaces_wet(nface[H_WET], fpout);
    pwet->pneigh_ele_V = WET_ini_peles_wet(nface[V_WET], fpout); // 25-05-2016 //elele v_WET H_WET
    pwet->pneigh_ele_H = WET_ini_peles_wet(nface[H_WET], fpout);
    ngeom = pwet->ngeom;
    for (ng = 0; ng < ngeom; ng++) {
      pgeom = pwet->p_geom[ng];
      nele = pgeom->nele;
      nlayer = MSH_get_layer_rank_by_name(pcmsh, pgeom->name_aq, fpout); // 25-05-2016
      for (e = 0; e < nele; e++) {
        pele = pgeom->p_ele[e];
        id_ele1 = pele->id[INTERN_MSH];
        for (idir = X_MSH; idir < ND_MSH; idir++) {
          for (itype = ONE; itype < NFACE_MSH; itype++) {
            pface = pele->face[idir][itype];
            if (idir != Z_MSH) {
              p_ele = pface->p_ele;
              // id_ele = p_ele[0]->id[INTERN_MSH] == id_ele ? p_ele[1]->id[INTERN_MSH] : id_ele1;
              if (p_ele[0]->id[INTERN_MSH] == id_ele1)
                id_ele = p_ele[1]->id[INTERN_MSH];
              else
                id_ele = p_ele[0]->id[INTERN_MSH];
              if (WET_check_face_aq_wet(pgeom, id_ele, fpout)) {
                pwet->p_faces_V[idface_V++] = pele->face[idir][itype];
                pwet->pneigh_ele_V[idele_V++] = MSH_get_ele(pcmsh, nlayer, id_ele, fpout); // 25-05-2016
                // LP_printf(fpout,"in WET_define_face_ele_aq_wet yes you are right! id_ele = %d cond = %f\n",pele->id[INTERN_MSH],pwet->p_faces_V[idface_V++]->phydro->pcond->conductance);
              }
            } else if (itype == ONE && ng == (ngeom - 1)) {
              pwet->p_faces_H[idface_H++] = pele->face[idir][itype];
              p_ele = pface->p_ele; // 25-05-2016
              // if(p_ele[0]->id[INTERN_MSH] == id_ele1) // bug SW 09/06/2016 ABS_MSH must be used
              // id_ele = p_ele[1]->id[INTERN_MSH];
              // else
              id_ele = p_ele[0]->id[INTERN_MSH];
              pwet->pneigh_ele_H[idele_H++] = MSH_get_ele(pcmsh, nlayer + 1, id_ele, fpout); // 25-05-2016 nlayee + 1, element under lake
            }
          }
        }
      }
    }
    // LP_printf(fpout,"id_face = %d idele = %d\n",idface,idele);
  }
  return;
}

int WET_check_face_aq_wet(s_geom_wet *pgeom, int id_ele, FILE *fpout) {
  int nele, e;
  s_ele_msh *pele;

  nele = pgeom->nele;
  for (e = 0; e < nele; e++) {
    pele = pgeom->p_ele[e];
    if (id_ele == pele->id[INTERN_MSH])
      return 0;
  }
  return 1;
}

/*void WET_get_in_outflux_wet(s_lake_wet *pwet,FILE *fpout)
{
   double influx = 0,outflux = 0;
   int nface;
   int nf;
   s_face_msh **p_faces;
   s_ele_msh **pneigh_ele;

   nface = pwet->nface;
   p_faces = pwet->p_faces;
   pneigh_ele = pwet->pneigh_ele;


   for(nf = 0; nf < nface; nf++)
   {
       influx += p_faces[nf]->pflux->influx;
       outflux += p_faces[nf]->pflux->outflux;//25-05-2016
       //LP_printf(fpout,"conduc = %f\n",p_faces[nf]->phydro->pcond->conductance);
   }

   LP_printf(fpout,"The influx of lake %d is %f and outflux is %f\n",pwet->id_wet,influx,outflux);
   return;
}*/
