/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_bundary_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"
#include "GC.h"
#include "AQ.h"
#include "WET.h"

/**
 * @brief calculate h_lake for next time step refresh h_lake of all elements in all lakes
 */
void WET_define_dirichlet(s_cmsh *pcmsh, s_carac_wet *pcarac_wet, FILE *fpout) {
  int nw, ng, e;
  int nwet, ngeom, nele;
  int id_ele, id_layer;
  double h_ini;
  int kindof = DIRICHLET;
  s_id_io *id_list;
  s_lake_wet *pwet;
  s_geom_wet *pgeom;
  s_ft *pft;
  nwet = pcarac_wet->nwet;
  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    ngeom = pwet->ngeom;
    h_ini = pwet->phydro->h[INI_WET];
    pft = TS_create_function(0.0, h_ini);
    for (ng = 0; ng < ngeom; ng++) {
      pgeom = pwet->p_geom[ng];
      nele = pgeom->nele;
      for (e = 0; e < nele; e++) {
        id_ele = pgeom->p_ele[e]->id[INTERN_MSH];
        id_layer = MSH_get_layer_rank_by_name(pcmsh, pgeom->name_aq, fpout);
        id_list = IO_create_id(id_layer, id_ele);
        AQ_define_bound(pcmsh, id_list, pft, CODE_WET, kindof, CODE_WET, CODE_WET, CODE_WET, CODE_WET, fpout);
      }
    }
  }
  return;
}