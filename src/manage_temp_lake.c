/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_temp_lake.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"
#include "GC.h"
#include "AQ.h"

#include "WET.h"

/**\fn s_temp_wet *WET_ini_temp_lake()
 * \brief initialiser une structure lake temporaire et qui retourne un pointer vers cette structure
 *  return pwet
 */
s_temp_wet *WET_ini_temp_lake(void) {
  s_temp_wet *ptempwet;

  ptempwet = new_temp_wet();
  bzero((char *)ptempwet, sizeof(s_temp_wet));

  return ptempwet;
}

s_temp_wet *WET_create_temp_lake(int id_wet, char *name_layer, int id_ele) {
  s_temp_wet *ptempwet;
  ptempwet = WET_ini_temp_lake();

  ptempwet->id_wet = id_wet;
  ptempwet->id_ele = id_ele;
  strcpy(ptempwet->name_aq, name_layer);

  return ptempwet;
}

/* \fn s_temp_wet *WET_chain_lake_bck(s_temp_wet *ptemp1,s_temp_wet *ptemp2)
 * \brief Links two graviers with structure s_temp_wet
 *
 * retrun ptemp1
 */
s_temp_wet *WET_chain_temp_bck(s_temp_wet *ptemp1, s_temp_wet *ptemp2) {
  ptemp1->next = ptemp2;
  ptemp2->prev = ptemp1;
  return ptemp1;
}

/** \fn s_temp_wet *WET_secured_chain_temp_bck(s_temp_wet *ptemp1,s_temp_wet *ptemp2)
 * \brief chain gravier
 *
 * return ptemp
 *
 */
s_temp_wet *WET_secured_chain_temp_bck(s_temp_wet *ptemp1, s_temp_wet *ptemp2) {
  if (ptemp1 != NULL)
    ptemp1 = WET_chain_temp_bck(ptemp1, ptemp2);
  else
    ptemp1 = ptemp2;
  return ptemp1;
}

/** \fn s_temp_wet *WET_chain_temp_fwd(s_temp_wet *ptemp1,s_temp_wet *ptemp2)
 * \brief Links two graviers with structure s_temp_wet
 *
 * retrun ptemp2
 */
s_temp_wet *WET_chain_temp_fwd(s_temp_wet *ptemp1, s_temp_wet *ptemp2) {
  ptemp1->next = ptemp2;
  ptemp2->prev = ptemp1;
  return ptemp2;
}

/** \fn s_temp_wet *WET_secured_chain_temp_fwd(s_temp_wet *ptemp1,s_temp_wet *ptemp2)
 * \brief chain gravier
 *
 * return ptemp
 *
 */
s_temp_wet *WET_secured_chain_temp_fwd(s_temp_wet *ptemp1, s_temp_wet *ptemp2) {
  if (ptemp1 != NULL)
    ptemp1 = WET_chain_temp_fwd(ptemp1, ptemp2);
  else
    ptemp1 = ptemp2;
  return ptemp1;
}

/**\fn s_temp_wet **WET_tab_temp(s_temp_wet *pele,int nwet,FILE *fpout)
 *\brief put temp in array.
 *
 * put ptemp->next in p_wet[i+1], chain ptemp is created before in input.y
 *\return p_wet
 */
s_temp_wet **WET_tab_temp(s_temp_wet *ptemp, int nwet, FILE *fpout) {

  int i;
  s_temp_wet **p_temp;
  s_temp_wet *ptmp;
  // fprintf(stdout,"done3\n"); // BL to debug
  ptmp = WET_browse_temp(ptemp, BEGINNING_TS);

  p_temp = (s_temp_wet **)malloc(nwet * sizeof(s_temp_wet *));
  // ptemp=WET_browse_temp(ptemp,BEGINNING_TS); //????
  // LP_printf(fpout,"\ntemp id : %d\n",ptemp->id_wet); BL to debug
  for (i = 0; i < nwet; i++) {
    // LP_printf(fpout,"ID_wet : %d\n",ptemp->id_wet); BL to debug
    p_temp[i] = ptmp;
    ptmp = ptmp->next;
  }
  return p_temp;
}

/**\fn s_ele_lake_wet *MSH_browse_lake(s_ele_lake_wet *pelewet,int iwhere)
 *\brief rewind or forward chained elements
 *
 * iwhere can be BEGINNING_TS or END_TS
 */
s_temp_wet *WET_browse_temp(s_temp_wet *ptemp, int iwhere) {
  s_temp_wet *ptmp, *preturn;
  ptmp = ptemp;

  while (ptmp != NULL) {
    preturn = ptmp;

    // fprintf(stdout,"done4\n"); // BL to debug
    switch (iwhere) {
    case BEGINNING_WET:
      ptmp = ptmp->prev;
      break;
    case END_WET:
      ptmp = ptmp->next;
      break;
    }
  }
  return preturn;
}

/**\fn s_lake_wet **WET_ini_tab_temp_lake(int nwet,FILE *fpout)
 *\brief put lake in array.
 * put pwet->next in p_wet[i+1], chain pwet is created before in input.y
 *\return p_wet
 */

s_temp_wet **WET_ini_tab_temp_lake(int nwet, FILE *fpout) {

  int i;
  s_temp_wet **ptemp_wet;

  ptemp_wet = (s_temp_wet **)malloc(nwet * sizeof(s_temp_wet *));

  for (i = 0; i < nwet; i++) {

    ptemp_wet[i] = WET_ini_temp_lake();
  }
  return ptemp_wet;
}

/** \fn s_id_io *IO_free_id(s_id_io *pid)
 * \brief Deallocating a single index pointeur
 */
s_temp_wet *WET_free_temp(s_temp_wet *ptempwet) {
  free(ptempwet);
  ptempwet = NULL;
  return ptempwet;
}

/** \fn s_id_io *IO_free_id(s_id_io *pid)
 * \brief Deallocating a full index serie
 */
s_temp_wet *WET_free_temp_serie(s_temp_wet *ptempwet, FILE *fp) {
  s_temp_wet *ptmp;

  ptempwet = WET_browse_temp(ptempwet, BEGINNING_WET);

  ptmp = ptempwet;
  while (ptempwet->next != NULL) {
    ptmp = ptempwet->next;
    ptempwet = WET_free_temp(ptempwet);
    ptempwet = ptmp;
  }
  ptempwet = WET_free_temp(ptempwet);

  if (ptempwet != NULL)
    LP_warning(fp, "lake temp pointer not properly deallocated\n");
  return ptempwet;
}
