/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: ito_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"

char *WET_name_attributs(int iname) {
  char *name;

  switch (iname) {
  case WET_ID_ATT:
    name = strdup("Identifiant of lake and elements");
    break;
  case WET_EVAP_ATT:
    name = strdup("Evaporation of lake");
    break;
  case WET_PREP_ATT:
    name = strdup("Precipitation of lake");
    break;
  case WET_H_INI_ATT:
    name = strdup("Initial water head of lake");
    break;
  case WET_Z_MAX_ATT:
    name = strdup("The max elevation of lake");
    break;
  case WET_Z_MIN_ATT:
    name = strdup("The min elevation of lake");
    break;
  }
  return name;
}

char *WET_name_param_attributs(int iname) {
  char *name;

  switch (iname) {
  case WET_NUM_ATT:
    name = strdup("Number of lakes");
    break;
  case WET_EPS_ATT:
    name = strdup("Eps for picard loop");
    break;
  case WET_NIT_ATT:
    name = strdup("Number of iteration for picard loop");
    break;
  case WET_THETA_ATT:
    name = strdup("Theta for calculate h_lake");
    break;
  }
  return name;
}
