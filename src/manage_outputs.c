/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_output_mb_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"

/**
 * @brief Prints FORMATTED file headers depending to the wet output type
 */
void WET_print_headers(FILE *fout, int itype, FILE *flog) {

  // Common string to all types
  fprintf(fout, "DAY ID_WET ");

  switch (itype) {
  case MBWET_IO: {
    fprintf(fout, " PRECIP_m3 EVAPO_m3 RUNOFF_m3 INFLOW_m3 INFLOW_V_m3 INFLOW_H_m3 OUTFLOW_m3 OUTFLOW_V_m3 OUTFLOW_H_m3 SEEPAGE_m3 VOL_CHANGE_m3 RELATIVE_ERR_prct");
    break;
  }

  case HWET_IO: {
    fprintf(fout, "H_WET_m");
    break;
  }
  default: {
    LP_error(flog, "libwet%4.2f : Error in file %s, function %s at line %d : Unknown output type.\n", VERSION_WET, __FILE__, __func__, __LINE__);
    break;
  }
  }
  fprintf(fout, "\n");
}

/**
 * @brief Lauches the writing of the output files
 */
void WET_print_outputs(s_chronos_CHR *chronos, double t_day, s_carac_wet *pcarac_wet, s_out_io *pout, int out_type, FILE *fp) {

  switch (out_type) {
  case MBWET_IO: {
    WET_write_mb_wet(chronos->dt, t_day, pout, pcarac_wet, fp);
    break;
  }
  case HWET_IO: {
    WET_write_h_wet(t_day, pout, pcarac_wet, fp);
    break;
  }
  default: {
    LP_error(fp, "libwet%4.2f : Error in file %s, function %s at line %d : Unknown output type.\n", VERSION_WET, __FILE__, __func__, __LINE__);
    break;
  }
  }
}

/**
 * @brief Writes the content of the H_WET output file
 */
void WET_write_h_wet(double t_day, s_out_io *pout, s_carac_wet *pcarac_wet, FILE *flog) {

  int i, nw, nwet = pcarac_wet->nwet;
  double **values;
  s_lake_wet *pwet;

  values = (double **)malloc(NH_OUT_WET * sizeof(double *));
  for (i = 0; i < NH_OUT_WET; i++)
    values[i] = (double *)calloc(nwet, sizeof(double));

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    values[H_OUT_WET][nw] = pwet->phydro->h[T_WET];
  }

  if (pout->format == UNFORMATTED_IO)
    IO_print_bloc_bin(values, nwet, NH_OUT_WET, pout->fout);
  else { // FORMATTED

    for (nw = 0; nw < nwet; nw++) {
      fprintf(pout->fout, "%f %d ", t_day, pcarac_wet->p_wet[nw]->id_wet);
      for (i = 0; i < NH_OUT_WET; i++) {
        fprintf(pout->fout, "%f ", values[i][nw]);
      }
      fprintf(pout->fout, "\n");
    }
  }

  for (i = 0; i < NH_OUT_WET; i++)
    free(values[i]);

  free(values);
}

/**
 * @brief Writes the content of the MB_WET output file
 */
void WET_write_mb_wet(double dt, double t_day, s_out_io *pout, s_carac_wet *pcarac_wet, FILE *flog) {

  int i, nw, nwet = pcarac_wet->nwet;
  s_lake_wet *pwet;
  double **values;
  double denom = 0.;

  values = (double **)malloc(N_MB_OUT_WET * sizeof(double *));
  for (i = 0; i < N_MB_OUT_WET; i++)
    values[i] = (double *)calloc(nwet, sizeof(double));

  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];

    values[MBWET_PRE][nw] = pwet->phydro->q[WET_PRE] * pwet->surf * dt; // all outputs in m3 per time step  (NG : non homogeneous with other outputs which are in m3/s.)
    values[MBWET_EVAP][nw] = pwet->phydro->q[WET_EVAP] * pwet->surf * dt;
    values[MBWET_RUN][nw] = pwet->phydro->q[WET_RUN] * pwet->surf * dt;

    values[MBWET_INFLUX_V][nw] = pwet->phydro->q[WET_INFLUX_V] * dt;
    values[MBWET_INFLUX_H][nw] = pwet->phydro->q[WET_INFLUX_H] * dt;
    values[MBWET_INFLUX][nw] = values[MBWET_INFLUX_V][nw] + values[MBWET_INFLUX_H][nw];

    values[MBWET_OUTFLUX_V][nw] = pwet->phydro->q[WET_OUTFLUX_V] * dt;
    values[MBWET_OUTFLUX_H][nw] = pwet->phydro->q[WET_OUTFLUX_H] * dt;
    values[MBWET_OUTFLUX][nw] = values[MBWET_OUTFLUX_V][nw] + values[MBWET_OUTFLUX_H][nw];

    values[MBWET_LEAKAGE][nw] = pwet->phydro->q[WET_LEAKAGE] * dt;
    values[MBWET_DEL_V][nw] = pwet->phydro->q[WET_DEL_V];

    // Adding check on denominator to avoid nan strings
    if (values[MBWET_DEL_V][nw] < 0.) { // in %
      denom = values[MBWET_PRE][nw] + values[MBWET_RUN][nw] + values[MBWET_INFLUX][nw] - values[MBWET_DEL_V][nw];
      if (fabs(denom) > EPS_DEFAULT_WET)
        values[MBWET_REL_ERROR][nw] = 100 * (values[MBWET_PRE][nw] + values[MBWET_RUN][nw] + values[MBWET_INFLUX][nw] - values[MBWET_EVAP][nw] - values[MBWET_OUTFLUX][nw] - values[MBWET_DEL_V][nw]) / denom;
    } else {
      denom = values[MBWET_PRE][nw] + values[MBWET_RUN][nw] + values[MBWET_INFLUX][nw];
      if (fabs(denom) > EPS_DEFAULT_WET)
        values[MBWET_REL_ERROR][nw] = 100 * (values[MBWET_PRE][nw] + values[MBWET_RUN][nw] + values[MBWET_INFLUX][nw] - values[MBWET_EVAP][nw] - values[MBWET_OUTFLUX][nw] - values[MBWET_DEL_V][nw]) / denom;
    }
  }

  if (pout->format == UNFORMATTED_IO)
    IO_print_bloc_bin(values, nwet, N_MB_OUT_WET, pout->fout);
  else {
    for (nw = 0; nw < nwet; nw++) {
      fprintf(pout->fout, "%f %d ", t_day, pcarac_wet->p_wet[nw]->id_wet);
      for (i = 0; i < N_MB_OUT_WET; i++) {
        fprintf(pout->fout, "%f ", values[i][nw]);
      }
      fprintf(pout->fout, "\n");
    }
  }

  for (i = 0; i < WET_NTYPE; i++)
    free(values[i]);

  free(values);
}