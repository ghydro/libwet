/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libwet
 * FILE NAME: manage_mto_wet.c
 *
 * CONTRIBUTORS: Shuaitao WANG, Thomas VERBECKE(*), Nicolas FLIPO, Nicolas GALLOIS
 * (*) Sorbonne University
 *
 * LIBRARY BRIEF DESCRIPTION: Surface water bodies mass balance calculations
 * (lakes, gravel quarries, etc.) accounting for exchanges with atmosphere
 * and aquifer system.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libwet Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"

void WET_ini_mto_wet(s_carac_wet *pcarac_wet, int nmto, FILE *fpout) {

  int nwet, nw; // nombre de jours de simulation
  int i, nj;
  s_lake_wet *pwet;

  nwet = pcarac_wet->nwet;
  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    pwet->pmto = new_mto_wet();
    pwet->pmto->pftmto = (double **)malloc(NTYPE_MTO_WET * sizeof(double *));
    for (i = 0; i < NTYPE_MTO_WET; i++) {
      pwet->pmto->pftmto[i] = (double *)malloc(nmto * sizeof(double));
      for (nj = 0; nj < nmto; nj++) {

        pwet->pmto->pftmto[i][nj] = 0.0;
      }
    }
  }

  return;
}

void WET_free_mto_wet(s_carac_wet *pcarac_wet, FILE *fpout) {

  int nwet, nw; // nombre de jours de simulation
  int i;
  s_lake_wet *pwet;

  nwet = pcarac_wet->nwet;
  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    for (i = 0; i < NTYPE_MTO_WET; i++) {
      free(pwet->pmto->pftmto[i]);
      pwet->pmto->pftmto[i] = NULL;
    }
  }
  free(pwet->pmto->pftmto);
  pwet->pmto->pftmto = NULL;

  return;
}

/** fn calculate mean evaporation and precipitation for steady simulation*/

void WET_series_to_mean_mto(s_carac_wet *pcarac_wet, FILE *fpout) {
  int nwet, nw;
  int j, nj;

  double mean_e, mean_p; // evaporation precipitation moyenne journalère
  s_lake_wet *pwet;

  nwet = pcarac_wet->nwet;
  nj = pcarac_wet->length_mto;
  for (nw = 0; nw < nwet; nw++) {
    mean_e = 0.0;
    mean_p = 0.0;
    pwet = pcarac_wet->p_wet[nw];
    for (j = 0; j < nj; j++) {
      mean_e += pwet->pmto->pftmto[EVAPORATION_WET][j];
      mean_p += pwet->pmto->pftmto[PRECIPIT_WET][j];
    }
    pwet->phydro->q[WET_EVAP] = mean_e / nj;
    pwet->phydro->q[WET_PRE] = mean_p / nj;

    if (fabs(pwet->phydro->q[WET_PRE]) < EPS_LIMIT_WET || fabs(pwet->phydro->q[WET_EVAP]) < EPS_LIMIT_WET)
      LP_error(fpout, "evaporation or precipitation value is zero no input data_mto for id_wet = %d\n", nw + 1);
    LP_printf(fpout, "Mean evaporation is %e precipitation is %e\n", pwet->phydro->q[WET_EVAP], pwet->phydro->q[WET_PRE]);
  }
  return;
}

/** fn calculate evaporation series by Penman equation*/
void WET_calc_penman_transient_mto(s_carac_wet *pcarac_wet, int day, FILE *fpout) {
  int nwet, nw, id_wet;
  int j, nj;
  double evap, t_min_K, t_max_K, t_min_C, t_max_C, t_mean_K, t_mean_C, wind2, wind10, rh_min, rh_max, precip;
  double es, es_min, es_max, ea;  // tension de vapeur saturante es et tension de vapeur réelle ea
  double Rsw, Rlw, Rnw, Rn, patm; // net radiation in W/m2 Rnw and MJ/m2/d Rn
  double slop_pressure, lambda;
  double pyschrometric_const;

  s_lake_wet *pwet;

  nwet = pcarac_wet->nwet;
  nj = pcarac_wet->length_mto;
  j = day;
  if (j <= nj) {
    for (nw = 0; nw < nwet; nw++) {
      pwet = pcarac_wet->p_wet[nw];

      fscanf(pcarac_wet->fpmto, "%d %lf %lf %lf %lf %lf %lf %lf %lf %lf", &id_wet, &t_min_K, &t_max_K, &wind10, &rh_min, &rh_max, &Rsw, &Rlw, &patm, &precip);
      printf("mto_wet %d %lf %lf %lf %lf %lf %lf %lf %lf %lf \n", id_wet, t_min_K, t_max_K, wind10, rh_min, rh_max, Rsw, Rlw, patm, precip); // TV2020
      t_mean_K = (t_min_K + t_max_K) / 2;
      t_min_C = t_min_K - T_REF; // convertir en C T_REF refefrence 273.16
      t_max_C = t_max_K - T_REF;
      t_mean_C = t_mean_K - T_REF;
      wind2 = wind10 * 4.87 / log(67.8 * 10 - 5.74); // vitesse du vent a 10 m, convertir a 2 m en m/s
      if (fabs(patm) < EPS_LIMIT_WET) {
        patm = PATM; // PATM pression atmospherique standard 101.325 Kpa
        LP_warning(fpout, "No atmopheric pressure input data (0), take  standard atmopheric pressure 101.325 Kpa");
      }
      Rnw = (1 - ALBEDO) * Rsw + Rlw - EMISSIVITY * SIGMA * pow(t_mean_K, 4); // in W/m2
      Rn = 0.0864 * Rnw;                                                      // convertir W/m2 en MJ/m2/d
      if (Rn < 0)
        Rn = 0;
      es_min = 0.6108 * exp(17.27 * t_min_C / (t_min_C + 237.3)); // la tension de vapeur saturante en Kpa
      es_max = 0.6108 * exp(17.27 * t_max_C / (t_max_C + 237.3));
      es = (es_min + es_max) / 2;
      ea = (rh_max * es_min / 100.0 + rh_min * es_max / 100.0) / 2; // tension de vapeur relle en Kpa
      // ea = (rh_max + rh_min)*es/2/100;
      slop_pressure = 4098 * es / ((t_mean_C + 237.3) * (t_mean_C + 237.3)); // en Kp/C

      lambda = 2.501 - 2.361 * 0.001 * t_mean_C;                                                                                                                                      // the latent heat of vaporization in MJ/kg
      pyschrometric_const = 0.0016286 * patm / lambda;                                                                                                                                // en Kp/C patm en Kpa
      evap = slop_pressure * Rn / (slop_pressure + pyschrometric_const) / lambda + pyschrometric_const * (1.313 + 1.381 * wind2) * (es - ea) / (slop_pressure + pyschrometric_const); // evaporation in mm/d
      pwet->phydro->q[WET_EVAP] = evap * 0.001 / 86400;                                                                                                                               // mm/d to m/s
      pwet->phydro->q[WET_PRE] = precip / 1000;                                                                                                                                       // kg/m2/s to m/s
      if (evap < 0.0)
        LP_error(fpout, "for id_wet = %d day %d t_mean_k = %f evapration = %f mm/d es = %f ea = %f  rh_min = %f  rh_max =%f rn = %f\n", pwet->id_wet, j + 1, t_mean_K, evap, es, ea, rh_min, rh_max, Rn);
    }
  }

  return;
}

// TV 23/01/2020: fonction pour lire l'evap dans un fichier
//                prochaine version sans donnees mto (seulement precip et evap).
void WET_read_evap_mto(s_carac_wet *pcarac_wet, int day, FILE *fpout) {
  int nwet, nw, id_wet;
  int j, nj;
  double evap, precip;
  // double evap, t_min_K, t_max_K, t_min_C, t_max_C, t_mean_K, t_mean_C, wind2, wind10, rh_min, rh_max,precip;
  // double es, es_min, es_max, ea; // tension de vapeur saturante es et tension de vapeur réelle ea
  // double Rsw, Rlw, Rnw, Rn, patm;// net radiation in W/m2 Rnw and MJ/m2/d Rn
  // double slop_pressure, lambda;
  // double pyschrometric_const;

  s_lake_wet *pwet;
  nwet = pcarac_wet->nwet;
  nj = pcarac_wet->length_mto;
  j = day;
  if (j <= nj) {
    for (nw = 0; nw < nwet; nw++) {
      pwet = pcarac_wet->p_wet[nw];
      fscanf(pcarac_wet->fpmto, "%d %lf %lf", &id_wet, &precip, &evap);
      pwet->phydro->q[WET_EVAP] = evap;         // m/s
                                                // pwet->phydro->q[WET_EVAP] = evap*0.001/86400; //mm/d to m/s
      pwet->phydro->q[WET_PRE] = precip / 1000; // kg/m2/s to m/s
    }
  }
  return;
}

double *WET_get_evp_p_from_date(s_lake_wet *pwet, int length_mto, int nday, FILE *fpout) {
  int j, nj;
  int flag = NO_WET;
  double *value;
  double julian;
  value = (double *)malloc(2 * sizeof(double));
  value[WET_PRE] = 0.0;
  value[WET_EVAP] = 0.0;
  nj = length_mto;
  if (nday > nj)
    LP_error(fpout, "Error in length climat data simul day = %d length_mto = \n", nday, nj);

  value[WET_PRE] = pwet->pmto->pftmto[PRECIPIT_WET][nday - 1];
  value[WET_EVAP] = pwet->pmto->pftmto[EVAPORATION_WET][nday - 1];

  return value;
}

void WET_print_evap_penman_to_file(s_carac_wet *pcarac_wet, FILE *flog) {
  char *name;
  char *name_file;
  char *name_out;
  int j, nj, nw, nwet;
  s_lake_wet *pwet;
  FILE *fpmto;
  name_out = "WET_mto";
  name = (char *)malloc(STRING_LENGTH_LP * sizeof(char));

  sprintf(name, "%s/%s", getenv("RESULT"), name_out);
  name_file = (char *)malloc(STRING_LENGTH_LP * sizeof(char));
  sprintf(name_file, "%s.txt", name);
  LP_printf(flog, " opening file %s\n", name_file);

  fpmto = fopen(name_file, "w+");
  if (fpmto == NULL) {
    LP_error(flog, "cannot open the file %s\n", name_file);
  }
  nwet = pcarac_wet->nwet;
  nj = pcarac_wet->length_mto;
  for (nw = 0; nw < nwet; nw++) {
    pwet = pcarac_wet->p_wet[nw];
    for (j = 0; j < nj; j++) {
      fprintf(fpmto, "%d %e\n", nw + 1, pwet->pmto->pftmto[EVAPORATION_WET][j]);
    }
  }
  fclose(fpmto);
  free(name);
  free(name_file);

  return;
}

void WET_open_mto_file(s_carac_wet *pcarac_wet, char *name, char **name_out_folder, int folder_nb, FILE *fpout) {

  char *new_name;
  int i;
  for (i = 0; i < folder_nb; i++) {
    new_name = (char *)calloc(STRING_LENGTH_LP, sizeof(char));
    sprintf(new_name, "%s/%s", name_out_folder[i], name);
    fprintf(stderr, "try %d opening %s\n", i, new_name);
    pcarac_wet->fpmto = fopen(new_name, "r");
    free(new_name);
    if (pcarac_wet->fpmto != NULL)
      break;
  }

  if (pcarac_wet->fpmto == NULL) {
    LP_error(fpout, "Can't open the file %s. Please check the input path.\n", name);
  }

  return;
}

void WET_calc_penman_steady_mto(s_carac_wet *pcarac_wet, FILE *fpout) {

  int nwet, nw, id_wet;
  int j, nj;
  double evap, t_min_K, t_max_K, t_min_C, t_max_C, t_mean_K, t_mean_C, wind2, wind10, rh_min, rh_max, precip;
  double es, es_min, es_max, ea;  // tension de vapeur saturante es et tension de vapeur réelle ea
  double Rsw, Rlw, Rnw, Rn, patm; // net radiation in W/m2 Rnw and MJ/m2/d Rn
  double slop_pressure, lambda;
  double pyschrometric_const;

  s_lake_wet *pwet;

  nwet = pcarac_wet->nwet;
  nj = pcarac_wet->length_mto;

  for (j = 0; j < nj; j++) {
    for (nw = 0; nw < nwet; nw++) {
      pwet = pcarac_wet->p_wet[nw];

      fscanf(pcarac_wet->fpmto, "%d %lf %lf %lf %lf %lf %lf %lf %lf %lf", &id_wet, &t_min_K, &t_max_K, &wind10, &rh_min, &rh_max, &Rsw, &Rlw, &patm, &precip);

      t_mean_K = (t_min_K + t_max_K) / 2;
      t_min_C = t_min_K - T_REF; // convertir en C T_REF refefrence 273.16
      t_max_C = t_max_K - T_REF;
      t_mean_C = t_mean_K - T_REF;
      wind2 = wind10 * 4.87 / log(67.8 * 10 - 5.74); // vitesse du vent a 10 m, convertir a 2 m en m/s
      if (fabs(patm) < EPS_LIMIT_WET) {
        patm = PATM; // PATM pression atmospherique standard 101.325 Kpa
        LP_warning(fpout, "No atmopheric pressure input data (0), take  standard atmopheric pressure 101.325 Kpa");
      }
      Rnw = (1 - ALBEDO) * Rsw + Rlw - EMISSIVITY * SIGMA * pow(t_mean_K, 4); // in W/m2
      Rn = 0.0864 * Rnw;                                                      // convertir W/m2 en MJ/m2/d
      if (Rn < 0)
        Rn = 0;
      es_min = 0.6108 * exp(17.27 * t_min_C / (t_min_C + 237.3)); // la tension de vapeur saturante en Kpa
      es_max = 0.6108 * exp(17.27 * t_max_C / (t_max_C + 237.3));
      es = (es_min + es_max) / 2;
      ea = (rh_max * es_min / 100.0 + rh_min * es_max / 100.0) / 2; // tension de vapeur relle en Kpa
      // ea = (rh_max + rh_min)*es/2/100;
      slop_pressure = 4098 * es / ((t_mean_C + 237.3) * (t_mean_C + 237.3)); // en Kp/C

      lambda = 2.501 - 2.361 * 0.001 * t_mean_C;                                                                                                                                      // the latent heat of vaporization in MJ/kg
      pyschrometric_const = 0.0016286 * patm / lambda;                                                                                                                                // en Kp/C patm en Kpa
      evap = slop_pressure * Rn / (slop_pressure + pyschrometric_const) / lambda + pyschrometric_const * (1.313 + 1.381 * wind2) * (es - ea) / (slop_pressure + pyschrometric_const); // evaporation in mm/d
      // LP_printf(fpout,"int steady, your are right!\n");
      pwet->pmto->pftmto[EVAPORATION_WET][j] = evap * 0.001 / 86400; // mm/d to m/s
      pwet->pmto->pftmto[PRECIPIT_WET][j] = precip / 1000;           // kg/m2/s to m/s

      if (evap < 0.0)
        LP_printf(fpout, "for id_wet = %d day %d t_mean_k = %f evapration = %f mm/d es = %f ea = %f  rh_min = %f  rh_max =%f rn = %f\n", pwet->id_wet, j + 1, t_mean_K, evap, es, ea, rh_min, rh_max, Rn);
    }
  }
  return;
}
